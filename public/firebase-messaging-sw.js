importScripts("https://www.gstatic.com/firebasejs/7.24.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/7.24.0/firebase-messaging.js");
const firebaseConfig = {
  apiKey: "AIzaSyC_Pl5udaAwtATdcxsp5DyOXLlyyUDqm9w",
  authDomain: "taboorqms-ca965.firebaseapp.com",
  databaseURL: "https://taboorqms-ca965.firebaseio.com",
  projectId: "taboorqms-ca965",
  storageBucket: "taboorqms-ca965.appspot.com",
  messagingSenderId: "580234484527",
  appId: "1:580234484527:web:18d11cc374a4ea4e5c5fda",
  measurementId: "G-QS19PE9KN7"
};
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
     const promiseChain = clients
          .matchAll({
               type: "window",
               includeUncontrolled: true,
          })
          .then((windowClients) => {
               for (let i = 0; i < windowClients.length; i++) {
                    const windowClient = windowClients[i];
                    windowClient.postMessage(payload);
               }
          })
          .then(() => {
               return registration.showNotification("my notification title");
          });
     return promiseChain;
});
self.addEventListener("notificationclick", function(event) {
     console.log(event);
});