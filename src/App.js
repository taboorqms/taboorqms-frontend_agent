import React, { Component } from "react";
import { HashRouter, Route, Switch, Redirect } from "react-router-dom";
import "./App.scss";
import ls from "local-storage";
import { messaging } from "./init-fcm";
import { isLogin } from "./utils/index";
const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
);
const DefaultLayout = React.lazy(() => import("./containers/DefaultLayout"));
const Landing = React.lazy(() => import("./views/Pages/Landing"));
const Login = React.lazy(() => import("./views/Pages/Login"));
const Register = React.lazy(() => import("./views/Pages/Register"));
const Page404 = React.lazy(() => import("./views/Pages/Page404"));
const Page500 = React.lazy(() => import("./views/Pages/Page500"));
const ChangePassword = React.lazy(() => import("./views/Pages/Login/ChangePassword"));
class App extends Component {
  constructor(props) {
    super(props);
    global.url = "https://apicall.taboor.ae/taboor-qms";
    global.Language = localStorage.getItem('Language')?localStorage.getItem('Language'):"EN";
    this.state = {
      token: "",
    };
    console.log('==================Language==================');
    console.log(localStorage.getItem('Language'));
    console.log('===================Language=================');
  }
  async componentDidMount() {
    messaging
      .requestPermission()
      .then(async function () {
        const token = await messaging.getToken();
        global.deviceToken = token;
        await ls.set("deviceToken", token);
        console.log("deviceToken", token);
      }).catch(function (err) {
        console.log("Unable to get permission to notify.", err);
      });
    let token = await ls.get("token");
    this.setState({ token });
     navigator.serviceWorker.addEventListener("message", (message) => {
       let { ticketsWaitingInQueue } = message.data[
         "firebase-messaging-msg-data"
       ].data;
       console.log("recieving messageeee", message);
       ls.remove("ticketInQueue");
       ls.set("ticketInQueue", ticketsWaitingInQueue);
     });
     messaging.onMessage((payload) =>
       console.log("Message received. ", payload)
     );
  }
  render() {
    return (
      <HashRouter>
        <React.Suspense fallback={loading()}>
          <Switch>
            <Route
              exact
              path="/landing"
              name="Landing Page"
              render={(props) => <Landing {...props} />}
            />
            <Route
              exact
              path="/login"
              name="Login Page"
              render={(props) => <Login {...props} />}
            />
                        <Route
              exact
              path="/changePassword"
              name="Change Password Page"
              render={(props) => <ChangePassword {...props} />}
            />
            <Route
              exact
              path="/register"
              name="Register Page"
              render={(props) => <Register {...props} />}
            />
            <Route
              exact
              path="/404"
              name="Page 404"
              render={(props) => <Page404 {...props} />}
            />
            <Route
              exact
              path="/500"
              name="Page 500"
              render={(props) => <Page500 {...props} />}
            />
            <Route
              path="/"
              // name="Home"
              render={(props) => {
                console.log("in the home pageeeee", ls.get("token"));
                return ls.get("token") ? (
                  <DefaultLayout {...props} />
                ) : (
                  <Redirect to="/login" />
                );
              }}
            />
          </Switch>
        </React.Suspense>
      </HashRouter>
    );
  }
}

export default App;
