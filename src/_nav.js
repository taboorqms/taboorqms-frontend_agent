export default {
  items: [
    // {
    //   title: true,
    //   name: "Theme",
    //   wrapper: {
    //     // optional wrapper object
    //     element: "", // required valid HTML5 element tag
    //     attributes: {}, // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
    //   },
    //   class: "", // optional class names space delimited list for title item ex: "text-center"
    // },

    // {
    //   title: true,
    //   name: "Components",
    //   wrapper: {
    //     element: "",
    //     attributes: {},
    //   },
    // },
    {
      name: "Lorem ipsum ddsadasdasdasdsa",
      url: "/base",
     
      badge: {
        variant: "pill",
        text: "3 Req. Docs",
        icon: "icon-doc",
      },
      children: [
        {
          name: "Landing",
          url: "/landing",
        },
        {
          name: "Login",
          url: "/login",
        },
      ],
    },
    {
      name: "Lorem ipsum dolor sitamet consectetur adipiscing",
      url: "/buttons",
      icon: "icon-doc",
      badge: {
        variant: "pill",
        text: "2 Req. Docs",
      },
      children: [
        {
          name: "Landing",
          url: "/landing",
        },
        {
          name: "Login",
          url: "/login",
        },
      ],
    },
    {
      name: "Lorem ipsum dolor sitamet consectetur adipiscing",
      url: "/charts",
      icon: "icon-doc",
      badge: {
        variant: "pill",
        text: "3 Req. Docs",
      },
      children: [
        {
          name: "Birth Certificate",
          url: "/landing",
        },
        {
          name: "Passport",
          url: "/login",
        },
        {
          name: "Bank Statement",
          url: "/login",
        },
      ],
    },
  ],
};
