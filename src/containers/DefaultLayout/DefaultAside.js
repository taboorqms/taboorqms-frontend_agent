import React, { Component } from 'react';
import { Nav, NavItem, NavLink, Progress, TabContent, TabPane, ListGroup, ListGroupItem } from 'reactstrap';
import { AppAsideToggler, AppSidebarToggler } from '@coreui/react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { AppSwitch } from '@coreui/react'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultAside extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <Nav tabs>
          <NavItem>
            <NavLink className={classNames({ active: this.state.activeTab === '1' })}
                     onClick={() => {
                       this.toggle('1');
                     }}>
              <i className="icon-user"></i> Profile 
              <a className="float-right">
                <AppAsideToggler className="icon-close p-0" />
              </a>
              <div className="clearfix"></div>
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1" className="p-3">

            <div className="text-center mb-3">
              <div className="profile-logo">Logo</div>
              <h5>Service Center Name</h5>
              <a href="#"><i className="icon-location-pin"></i> Lorem Ipsum</a>
            </div>
            
            <div className="row mb-3">
              <div className="col-6">
                     <p>
                       Created On <br></br>
                       <strong>02.05.2019</strong>
                     </p>
              </div>
              <div className="col-6">
                     <p>
                      Category <br></br>
                       <strong>Service type</strong>
                     </p>
              </div>
            </div>

          <div className="card mb-3">
            <div className="card-body text-center p-2">
            <div className="row">
              <div className="col-6">
                  <h2 className="text-warning">24</h2>
                  <p><i className="nav-icon icon-present"></i> Branches</p>
              </div>
              <div className="col-6">
                  <h2 className="text-warning">38</h2>
                  <p><i className="nav-icon icon-user"></i> Agents</p>
              </div>
            </div>
            </div>
          </div>

                     <h4>Services <span class="badge badge-warning badge-pill">7</span></h4>
                     <ul className="list-dots">
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet Lorem ipsum doller sit amet </a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                     </ul>
           


          </TabPane>
        </TabContent>
      </React.Fragment>
    );
  }
}

DefaultAside.propTypes = propTypes;
DefaultAside.defaultProps = defaultProps;

export default DefaultAside;
