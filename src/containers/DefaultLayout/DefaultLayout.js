import React, { Component, Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import * as router from "react-router-dom";
import { Container } from "reactstrap";
import ls from "local-storage";
import { CircularProgress } from "@material-ui/core";

import base64 from "base-64";
import { Link, NavLink } from "react-router-dom";
import {
  Badge,
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem,
} from "reactstrap";

import {
  AppAsideToggler,
  AppNavbarBrand,
  AppSidebarToggler,
} from "@coreui/react";

import logo from "../../assets/img/brand/logo.png";

import {
  AppAside,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from "@coreui/react";
// sidebar nav config
import navigation from "../../_nav";
// routes config
import routes from "../../routes";

const DefaultAside = React.lazy(() => import("./DefaultAside"));
const DefaultFooter = React.lazy(() => import("./DefaultFooter"));
const DefaultHeader = React.lazy(() => import("./DefaultHeader"));

class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      agentData: [],
      serviceList: [],
      servingListData: [],
      profilepic:""
    };
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );

  signOut = async (e) => {
    e.preventDefault();
    //

    fetch(global.url + "/kcatmanagement/agent/logout", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await ls.get("token")),
      },
    })
      .then((response) => response.json())
      .then(async (res) => {
        if (res.applicationStatusCode == 0) {
          this.props.history.replace("/login");

          ls.remove("token");
        } else {
          alert(res.devMessage, [{ text: "OK" }], {
            cancelable: false,
          });
        }
      })
      .catch((error) => {
        alert(JSON.stringify(error));
      });
  };

  getProfile = async () => {
    fetch(global.url + "/kcatmanagement/agent/get/profile", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + ls.get("token"),
      },
    })
      .then((response) => response.json())
      .then(async (res) => {
        if(res.serviceCentrePic!='') {
          fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/fileData/get', {
            method: 'POST',
            body:res.serviceCentrePic,
            headers: { Authorization: "Bearer " + ls.get("token"), },
          }).then(response => response.json()).then(responsejson => {
            if(responsejson.applicationStatusCode==0) {
              this.setState({profilepic:responsejson.applicationStatusResponse});

            }
          })
        }
        console.log("resssssssssssssssssss ===serivice", res);
        let temp = [];
        temp.push(res);

        let obj = {};
        let servList = [];
        res.serviceList.map((item) => {
          let tempReq = [];
          item.requirements.map((req) => {
            tempReq.push({
              name:
                global.Language == "EN"
                  ? req.requirementName
                  : req.requirementNameArabic,
            });
            console.log("itemmm", item);
          });
          obj = {
            name: item.serviceName,
            // url: "/",

            badge: {
              variant: "pill",
              text: item.requirements.length + ". Docs",
            },
            icon: "icon-doc",
            children: [
              ...tempReq,
              // {
              //   name: req.requirementName,
              // },
              // {
              //   name: "Landing",
              //   // url: "/landing",
              // },
              // {
              //   name: "Login",
              //   // url: "/login",
              // },
            ],
          };
          console.log(
            "======================, res.serviceList",
            res.serviceList
          );

          console.log("======================,tempReq", tempReq);

          servList.push(obj);
        });
        this.setState({
          agentData: temp,
          serviceList: res.serviceList,
          servingListData: servList,
        });
      })
      .catch((error) => {
        // alert(JSON.stringify(error));
      });
  };
  componentDidMount() {
    this.getProfile();
  }
  render() {
    console.log("========seg list data", this.state.servingListData);
    return (
      <div className="app">
        {/* <AppHeader fixed>
          <Suspense  fallback={this.loading()}>
            <DefaultHeader onLogout={e=>this.signOut(e)}/>
          </Suspense>
        </AppHeader> */}
        {this.state.agentData.length != 0 ? (
          this.state.agentData.map((item) => (
            <div className="app-body">
              <AppSidebar fixed display="lg">
                <div className="side-logo">
                  {/* <AppNavbarBrand
              full={{ src: logo, width: 150, alt: 'Taboor Logo' }}
            /> */}
                  {this.state.profilepic != "" ? (
                      <img
                        style={{ height: 50, width: 50 ,objectFit:'contain',marginBottom:10}}
                        src={"data:image/png;base64," + this.state.profilepic}
                      />
                  ) : (
                      <img style={{ height: 50, width: 50 ,objectFit:'contain',marginBottom:10}}
                        src={require('../../assets/img/brand/taboor1.png')} />
                  )}

                  <h4>{item.branchName}</h4>
                  <p className="text-info">
                    <i class="icon-location text-info"></i>{" "}
                    {item.branchLocation}
                  </p>
                </div>
                {global.Language == "EN" ? (
                  <h5 className="no-wrap">
                    Available Services 1{" "}
                    <span class="badge bg-warning text-white">
                      {this.state.serviceList.length}
                    </span>
                  </h5>
                ) : (
                  <h5 className="no-wrap">
                    الخدمات المتاحة
                    <span class="badge bg-warning text-white">
                      {" "}
                      {this.state.serviceList.length}
                    </span>
                  </h5>
                )}

                <AppSidebarHeader />
                <AppSidebarForm />
                <Suspense>
                  <AppSidebarNav
                    navConfig={
                      this.state.servingListData.length !== 0
                        ? {
                            items: [...this.state.servingListData],
                          }
                        : navigation
                    }
                    // navConfig={navigation}
                    {...this.props}
                    router={router}
                  />
                </Suspense>
                <AppSidebarFooter />
                <AppSidebarMinimizer />
              </AppSidebar>
              <main className="main">
                <div className="admin-header">
                  <AppSidebarToggler className="d-md-down-none" display="lg" />
                  {/* <AppBreadcrumb
                    className="page-title"
                    appRoutes={routes}
                    router={router}
                  /> */}
                  {global.Language == "EN" ? (
                    <h2 className="page-title text-primary ml-3">
                      Counter {item.counterNumber}
                    </h2>
                  ) : (
                    <h2 className="page-title text-primary ml-3">
                      الشباك{item.counterNumber}
                    </h2>
                  )}
                  <Nav className="admin-nav" navbar>
                    <NavItem className="font-2xl text-light-grey">
                      {item.agentName}
                      <NavLink
                        to="/buttons/button-groups"
                        className="btn btn-grey btn-sm font-2xl font-weight-light ml-2 pl-3 pr-3"
                      >
                        {item.agentName.toUpperCase().charAt(0)}
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <Link
                        onClick={this.signOut}
                        className="ml-2 text-light-grey mt-2 d-block"
                      >
                        {" "}
                        <i className="fa-2x icon-signout"></i>
                      </Link>
                    </NavItem>
                  </Nav>
                  {/* <AppAsideToggler className="d-md-down-none" /> */}
                </div>

                <Container fluid>
                  <Suspense fallback={this.loading()}>
                    <Switch>
                      {routes.map((route, idx) => {
                        return route.component ? (
                          <Route
                            key={idx}
                            path={route.path}
                            exact={route.exact}
                            name={route.name}
                            render={(props) => <route.component {...props} />}
                          />
                        ) : null;
                      })}
                      <Redirect from="/" to="/dashboard" />
                    </Switch>
                  </Suspense>
                </Container>
              </main>
              <AppAside fixed>
                <Suspense fallback={this.loading()}>
                  <DefaultAside />
                </Suspense>
              </AppAside>
            </div>
          ))
        ) : (
          <div
            style={{
              flex: 1,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <CircularProgress />
          </div>
        )}
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
