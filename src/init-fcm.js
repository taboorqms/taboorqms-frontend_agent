import * as firebase from "firebase/app";
import "firebase/messaging";
const initializedFirebaseApp = firebase.initializeApp({
     // Project Settings => Add Firebase to your web app
     apiKey: "AIzaSyC_Pl5udaAwtATdcxsp5DyOXLlyyUDqm9w",
      authDomain: "taboorqms-ca965.firebaseapp.com",
      databaseURL: "https://taboorqms-ca965.firebaseio.com",
      projectId: "taboorqms-ca965",
      storageBucket: "taboorqms-ca965.appspot.com",
      messagingSenderId: "580234484527",
      appId: "1:580234484527:web:18d11cc374a4ea4e5c5fda",
      measurementId: "G-QS19PE9KN7"
});
const messaging = initializedFirebaseApp.messaging();
export { messaging };