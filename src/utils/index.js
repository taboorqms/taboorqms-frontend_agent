import ls from "local-storage";

export const login = (token) => {
  ls.set("token", token);
};

export const logout = () => {
  ls.remove("token");
};

export const isLogin = async () => {
  let token = await ls.get("token");
  if (token) {
    return true;
  }

  return false;
};
