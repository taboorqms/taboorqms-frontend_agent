import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import { Link } from 'react-router-dom';

class Breadcrumbs extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Row className="mb-3 hidden">
          <Col sm="6">
            <h3><input type="checkbox"></input> 24 Branches</h3>
          </Col>

          <Col sm="6" className="text-right">
          
            <div className="float-right">
            <button className="btn btn-outline-secondary ml-2">Filters <i className="fa fa-filter"></i></button>
            <button className="btn btn-outline-secondary ml-2">Sort By <i className="fa fa-sort-amount-asc"></i></button>
            <button className="btn btn-outline-warning ml-2"><i class="fa fa-plus"></i></button>
            </div>
            <div class="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" class="form-control b-r-0 bg-transparent"></input>
              <div class="input-group-append"><span class="input-group-text bg-transparent b-l-0"><i class="fa fa-search"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>
        </Row>



            <Card className="mb-0">
              
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                    <th></th>
                    <th className="text-center">Status</th>
                    <th>Branch</th>
                    <th>Services</th>
                    <th>Counters</th>
                    <th>Agents</th>
                    <th>AST</th>
                    <th>Added On</th>
                    <th>Rating</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    <td className="text-center">
                    <span class="badge badge-success">Open</span>
                    </td>
                    <td>Branch's Name</td>
                    <td>Lorem ipsum doller sit amet</td>
                    <td className="text-center">8</td>
                    <td className="text-center">2</td>
                    <td className="text-center">35 min</td>
                    <td className="text-center">12.4.19</td>
                    <td className="text-center">
                      <i className="fa fa-smile-o text-success f20"></i>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    <td className="text-center">
                    <span class="badge badge-success">Open</span>
                    </td>
                    <td>Branch's Name</td>
                    <td>Lorem ipsum doller sit amet</td>
                    <td className="text-center">8</td>
                    <td className="text-center">2</td>
                    <td className="text-center">35 min</td>
                    <td className="text-center">12.4.19</td>
                    <td className="text-center">
                      <i className="fa fa-smile-o text-success f20"></i>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    <td className="text-center">
                    <span class="badge badge-danger">Closed</span>
                    </td>
                    <td>Branch's Name</td>
                    <td>Lorem ipsum doller sit amet</td>
                    <td className="text-center">8</td>
                    <td className="text-center">2</td>
                    <td className="text-center">35 min</td>
                    <td className="text-center">12.4.19</td>
                    <td className="text-center">
                      <i className="fa fa-smile-o text-warning f20"></i>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    <td className="text-center">
                    <span class="badge badge-success">Open</span>
                    </td>
                    <td>Branch's Name</td>
                    <td>Lorem ipsum doller sit amet</td>
                    <td className="text-center">8</td>
                    <td className="text-center">2</td>
                    <td className="text-center">35 min</td>
                    <td className="text-center">12.4.19</td>
                    <td className="text-center">
                      <i className="fa fa-smile-o text-success f20"></i>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    <td className="text-center">
                    <span class="badge badge-success">Open</span>
                    </td>
                    <td>Branch's Name</td>
                    <td>Lorem ipsum doller sit amet</td>
                    <td className="text-center">8</td>
                    <td className="text-center">2</td>
                    <td className="text-center">35 min</td>
                    <td className="text-center">12.4.19</td>
                    <td className="text-center">
                      <i className="fa fa-smile-o text-success f20"></i>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    <td className="text-center">
                    <span class="badge badge-success">Open</span>
                    </td>
                    <td>Branch's Name</td>
                    <td>Lorem ipsum doller sit amet</td>
                    <td className="text-center">8</td>
                    <td className="text-center">2</td>
                    <td className="text-center">35 min</td>
                    <td className="text-center">12.4.19</td>
                    <td className="text-center">
                      <i className="fa fa-smile-o text-warning f20"></i>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    <td className="text-center">
                    <span class="badge badge-danger">Closed</span>
                    </td>
                    <td>Branch's Name</td>
                    <td>Lorem ipsum doller sit amet</td>
                    <td className="text-center">8</td>
                    <td className="text-center">2</td>
                    <td className="text-center">35 min</td>
                    <td className="text-center">12.4.19</td>
                    <td className="text-center">
                      <i className="fa fa-smile-o text-success f20"></i>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    <td className="text-center">
                    <span class="badge badge-success">Open</span>
                    </td>
                    <td>Branch's Name</td>
                    <td>Lorem ipsum doller sit amet</td>
                    <td className="text-center">8</td>
                    <td className="text-center">2</td>
                    <td className="text-center">35 min</td>
                    <td className="text-center">12.4.19</td>
                    <td className="text-center">
                      <i className="fa fa-smile-o text-warning f20"></i>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    <td className="text-center">
                    <span class="badge badge-success">Open</span>
                    </td>
                    <td>Branch's Name</td>
                    <td>Lorem ipsum doller sit amet</td>
                    <td className="text-center">8</td>
                    <td className="text-center">2</td>
                    <td className="text-center">35 min</td>
                    <td className="text-center">12.4.19</td>
                    <td className="text-center">
                      <i className="fa fa-smile-o text-success f20"></i>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  
                  </tbody>
                </Table>
               
              </CardBody>
            </Card>
          
          


      


      </div>
    );
  }
}

export default Breadcrumbs;
