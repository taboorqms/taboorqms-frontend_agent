import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Alert, Col, Row, Table, Button, FormGroup, Label, Input, ButtonGroup, ButtonDropdown, DropdownItem,  DropdownMenu,  DropdownToggle,ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';
import { Link } from 'react-router-dom';
import {Line, Pie } from 'react-chartjs-2';
import { AppSwitch } from '@coreui/react'
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';
import Steps, { Step } from 'rc-steps';

class Breadcrumbs extends Component {




  constructor(props) {
    super(props);

    

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
    };

    this.state={
      next:0
    }

    this.onDismiss = this.onDismiss.bind(this);


  }
  nextStep=(next)=>{
    this.setState({next:next});
  }
  
  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }



  onDismiss() {
    this.setState({ visible: false });
  }




  











  render() {
    return (
      <div className="animated fadeIn">
        <Row className="mb-3 hidden">
          
          <Col sm="6">
              <h4 className="mb-0">New Branch</h4>
            </Col>
              
              
          <Col sm="6" className="text-right">
          
          <Steps current={this.state.next}  direction="horizontal">
                <Steps.Step title="Details" />
                <Steps.Step title="Agents" />
                <Steps.Step title="Counters" />
              </Steps>
           
          </Col>
        </Row>


        <Card>
              
              <CardBody>
        


          {(this.state.next==0) && (
            <div className="steps-content">
                <Row>
          <Col sm="8">
            <h4>Basic</h4>
                <FormGroup className="input-line">
                    <Label className="text-light-grey">Branch Name</Label>
                    <Input type="text" placeholder="Service Center Name" autoComplete="username" />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Available Services</Label>
                    <select className="form-control">
                      <option>Banking</option>
                      <option>Option 2</option>
                    </select>
                  </FormGroup>
                  <Alert color="light" className="theme-alert mb-5" isOpen={this.state.visible} toggle={this.onDismiss}>
                      Fusce sodales quis metus eget diagnissim
                  </Alert>

                    <h6 className="text-light-grey">Working Hours</h6>
                  
                  <Row>
                    <Col sm="4">
                      <FormGroup className="input-line">
                      <Label className="text-light-grey">Week Day</Label>
                      <select className="form-control">
                        <option>Monday</option>
                        <option>Tuesday</option>
                        <option>Wednesday</option>
                        <option>Thursday</option>
                        <option>Friday</option>
                      </select>
                    </FormGroup>
                    </Col>
                    <Col sm="6">
                      <FormGroup className="input-line">
                      <Label className="text-light-grey">Time</Label>
                      <Row>
                        <Col sm="5">
                        <select className="form-control">
                        <option>09:00 AM</option>
                        <option>10:00 AM</option>
                        <option>11:00 AM</option>
                        <option>12:00 PM</option>
                        <option>01:00 PM</option>
                      </select>
                        </Col>
                        <Col sm="2">To</Col>
                        <Col sm="5">
                        <select className="form-control">
                        <option>09:00 AM</option>
                        <option>10:00 AM</option>
                        <option>11:00 AM</option>
                        <option>12:00 PM</option>
                        <option>01:00 PM</option>
                      </select>
                        </Col>
                      </Row>
                      
                    </FormGroup>
                    </Col>
                    
                    <Col sm="2" className="pt-3">
                      <Button outline color="warning">Add</Button>
                    </Col>
                  </Row>
                  <Alert color="light" className="theme-alert" isOpen={this.state.visible} toggle={this.onDismiss}>
                      <Row>
                        <Col sm="6">Sunday</Col>
                        <Col sm="6">10:00 AM - 03:00 PM</Col>
                      </Row>
                  </Alert>
                  <Alert color="light" className="theme-alert" isOpen={this.state.visible} toggle={this.onDismiss}>
                      <Row>
                        <Col sm="6">Sunday</Col>
                        <Col sm="6">10:00 AM - 03:00 PM</Col>
                      </Row>
                  </Alert>

              <Button color="primary" onClick={()=>{this.nextStep(1)}}>Save and Continue</Button>
              </Col>
          <Col sm="4">
          


          

                <p className="b-b-1 pb-2"><i className="fa fa-envelope-o text-warning"></i> info@lorem.com</p>
                <p className="mb-3 b-b-1 pb-2"><i className="fa fa-phone text-warning"></i> 00971 23 45678923</p>

                <div className="mb-2">
                  <iframe className="border-0 w-100" src="https://www.google.com/"></iframe>
                </div>

                <Button outline block color="warning" className="mb-3"><i className="fa fa-map-marker"></i> Edit Location</Button>
                <p>Lorem ipsum doller sit amet Lorem ipsum doller sit amet Lorem ipsum doller sit amet Lorem ipsum doller sit amet Lorem ipsum doller sit amet</p>

          </Col>
        
        </Row>
            </div>
            
            )}
          

          {(this.state.next==1) && (
              <div className="steps-content">
                <Row>
          <Col sm="8">

          <Card className="card-box">
              <CardBody className="pt-4">
              <Row>
                  <Col sm="8">
                  <h4>Select From Existing List</h4>
                  </Col>
                  <Col sm="4" className="text-right">
                      <Link className="text-warning f20"><i className="fa fa-plus"></i></Link>
                  </Col>
                </Row>
                </CardBody>
                </Card>

            <Card className="card-box mb-0">
              <CardBody className="pt-4">
              <Row className="mb-3">
                  <Col sm="8">
                  <h4>Create New Agent</h4>
                  </Col>
                  <Col sm="4" className="text-right">
                      <Link className="text-warning f20"><i className="fa fa-times"></i></Link>
                  </Col>
                </Row>
             
              <Row>
                <Col sm="6">
                <FormGroup className="input-line">
                    <Label className="text-light-grey">Agent ID</Label>
                    <Input type="text" autoComplete="username" />
                    <small><span className="text-danger">*</span> Generated by System</small>
                  </FormGroup>
                </Col>
                <Col sm="6">
                <FormGroup className="input-line">
                    <Label className="text-light-grey">PIN</Label>
                    <Input type="text" autoComplete="username" />
                    <small><span className="text-danger">*</span> Changed on first login</small>
                  </FormGroup>
                </Col>
              </Row>
              <FormGroup className="input-line">
                    <Label className="text-light-grey">Assigned Counters</Label>
                    <select className="form-control">
                      <option>Banking</option>
                      <option>Option 2</option>
                    </select>
                  </FormGroup>
                <FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label>
                    <Input type="text" autoComplete="username" />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Job Title</Label>
                    <Input type="text" autoComplete="username" />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Branch</Label>
                    <Input type="text" autoComplete="username" />
                  </FormGroup>
                  

                  
                  <h6>Privileges</h6>

              <ListGroup className="list-simple mb-3">
                  <ListGroupItem>
                    <AppSwitch className={'mx-1'} color={'success'} outline={'alt'} checked label dataOn={'\u2713'} dataOff={'\u2715'}/>
                    Quisque non lectus et nulla eliquet lacinia eget id augue
                  </ListGroupItem>
                  <ListGroupItem>
                    <AppSwitch className={'mx-1'} color={'success'} outline={'alt'} checked label dataOn={'\u2713'} dataOff={'\u2715'}/>
                    Quisque non lectus et nulla eliquet lacinia eget id augue
                  </ListGroupItem>
                  <ListGroupItem>
                    <AppSwitch className={'mx-1'} color={'success'} outline={'alt'} checked label dataOn={'\u2713'} dataOff={'\u2715'}/>
                    Quisque non lectus et nulla eliquet lacinia eget id augue
                  </ListGroupItem>
                  <ListGroupItem>
                    <AppSwitch className={'mx-1'} color={'danger'} outline={'alt'} checked label dataOn={'\u2715'} dataOff={'\u2713'}/>
                    Quisque non lectus et nulla eliquet lacinia eget id augue
                  </ListGroupItem>
                  <ListGroupItem>
                    <AppSwitch className={'mx-1'} color={'danger'} outline={'alt'} checked label dataOn={'\u2715'} dataOff={'\u2713'}/>
                    Quisque non lectus et nulla eliquet lacinia eget id augue
                  </ListGroupItem>
               
                </ListGroup>

            \
          


                  <Button outline color="primary" onClick={()=>{this.nextStep(0)}}>Back</Button>
                <Button color="primary" className="ml-2" onClick={()=>{this.nextStep(2)}}>Add Agent</Button>


              </CardBody>
            </Card>
            

              </Col>
          <Col sm="4">
          


          <Card className="bg-primary">
            
            <CardBody>

            <h4 className="mb-3">Agents . 2</h4>

              

              <Card className="card-box card-list">
                <CardHeader>
                <p className="text-warning mb-0">K7294</p>
                  <h5 className="text-white float-left">Agent Name</h5>
                  <div className="card-header-actions float-right">
                    <Link><i className="fa fa-pencil"></i></Link>
                    <Link className="ml-3"><i className="fa fa-times"></i></Link>
                  </div>
                </CardHeader>
                <CardBody>
                <Table responsive className="m-0 table-borderless table-sm">
                <thead>
                <tr>
                      <td width="35%">001 , 005</td>
                      <td></td>
                  </tr>
                  <tr>
                      <td width="35%">Privileges 3</td>
                      <td>Branch Name Here</td>
                  </tr>
                  </thead>
                  
                </Table>
                </CardBody>
              </Card>

              <Card className="card-box card-list">
                <CardHeader>
                <p className="text-warning mb-0">K7294</p>
                  <h5 className="text-white float-left">Agent Name</h5>
                  <div className="card-header-actions float-right">
                    <Link><i className="fa fa-pencil"></i></Link>
                    <Link className="ml-3"><i className="fa fa-times"></i></Link>
                  </div>
                </CardHeader>
                <CardBody>
                <Table responsive className="m-0 table-borderless table-sm">
                <thead>
                <tr>
                      <td width="35%">001 , 005</td>
                      <td></td>
                  </tr>
                  <tr>
                      <td width="35%">Privileges 3</td>
                      <td>Branch Name Here</td>
                  </tr>
                  </thead>
                  
                </Table>
                </CardBody>
              </Card>


              <Row>
                <Col sm="6"><Button outline block color="light">Cancel</Button></Col>
                <Col sm="6"><Button block color="light">Save & Continue</Button></Col>
              </Row>

              
              

           

            </CardBody>
          </Card>
        
          

         
          
          </Col>
        
        </Row>
            
              
            </div>
            
          )}
          

          {(this.state.next==2) && (
            <div className="steps-content">
              <Row>
        <Col sm="8">
          <h4>Add New Counter</h4>
              <FormGroup className="input-line">
                  <Label className="text-light-grey">Counter Number</Label>
                  <Input type="text" placeholder="Service Center Name" autoComplete="username" />
                </FormGroup>
                <h6><span className="text-danger">*</span> Generated by the system</h6>
                <FormGroup className="input-line">
                  <Label className="text-light-grey">Available Services</Label>
                  <select className="form-control">
                    <option>Choose from list</option>
                    <option>Banking</option>
                    <option>Option 2</option>
                  </select>
                </FormGroup>
                <Alert color="light" className="theme-alert" isOpen={this.state.visible} toggle={this.onDismiss}>
                      Fusce sodales quis metus eget diagnissim
                  </Alert>
                  <Alert color="light" className="theme-alert" isOpen={this.state.visible} toggle={this.onDismiss}>
                      Fusce sodales quis metus eget diagnissim
                  </Alert>

                <Button outline color="primary" onClick={()=>{this.nextStep(1)}}>Back</Button>
                <Button color="primary" className="ml-2" onClick={()=>{this.nextStep(2)}}>Add Counter</Button>
            </Col>
        <Col sm="4">
        


        <Card className="bg-primary">
            
            <CardBody>

            <h4 className="mb-3">Counters . 6</h4>

              

              <Card className="card-box card-list">
                <CardHeader>
                  <h5 className="text-white float-left">001</h5>
                  <div className="card-header-actions float-right">
                    <Link><i className="fa fa-pencil"></i></Link>
                    <Link className="ml-3"><i className="fa fa-times"></i></Link>
                  </div>
                </CardHeader>
                <CardBody>
                <Table responsive className="m-0 table-borderless table-sm">
                <thead>
                <tr>
                      <td width="35%">Services 3</td>
                      <td>Agents 3</td>
                  </tr>
                  </thead>
                  
                </Table>
                </CardBody>
              </Card>

              <Card className="card-box card-list">
                <CardHeader>
                  <h5 className="text-white float-left">002</h5>
                  <div className="card-header-actions float-right">
                    <Link><i className="fa fa-pencil"></i></Link>
                    <Link className="ml-3"><i className="fa fa-times"></i></Link>
                  </div>
                </CardHeader>
                <CardBody>
                <Table responsive className="m-0 table-borderless table-sm">
                <thead>
                <tr>
                      <td width="35%">Services 2</td>
                      <td>Agents 3</td>
                  </tr>
                  </thead>
                  
                </Table>
                </CardBody>
              </Card>

              <Row>
                <Col sm="6"><Button outline block color="light">Cancel</Button></Col>
                <Col sm="6"><Button block color="light">Save</Button></Col>
              </Row>

              
              

           

            </CardBody>
          </Card>
        
          

        
        
        </Col>
      
      </Row>
          
            
          </div>
          
             
              

            
          )}



          
            </CardBody>
            </Card>
          


      


      </div>
    );
  }
}

export default Breadcrumbs;
