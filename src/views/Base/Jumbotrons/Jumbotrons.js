import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table, TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';

class Breadcrumbs extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: new Array(4).fill('1'),
    };
  }

 

  toggle(tabPane, tab) {
    const newArray = this.state.activeTab.slice()
    newArray[tabPane] = tab
    this.setState({
      activeTab: newArray,
    });
  }

  


  render() {
    return (
      <div className="animated fadeIn">
        
        <div className="tabs-line">

        <Nav tabs>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '1'}
                  onClick={() => { this.toggle(0, '1'); }}
                >
                  <h5 className="mb-0">M4873</h5>
                  <p><i class="fa fa-calendar text-light-grey"></i> 8:00 PM - 12.10.19</p>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '2'}
                  onClick={() => { this.toggle(0, '2'); }}
                >
                  <h5 className="mb-0">M4874</h5>
                  <p><i class="fa fa-calendar text-light-grey"></i> 8:00 PM - 12.10.19</p>
                </NavLink>
              </NavItem>
              
            </Nav>
            <TabContent className="pt-4" activeTab={this.state.activeTab[0]}>
            <TabPane tabId="1">
                
              
            <Row>
              <Col sm="3">
                <h4 className="mb-0">Branch's Name</h4>
                <p><i class="fa fa-map-marker text-light-grey"></i> Location here. <Link>Open Map</Link></p>
              </Col>
              <Col sm="2">
                <p className="mb-1"><i class="fa fa-ticket text-light-grey"></i> <b>Total #Tickets 8</b></p>
                <p><i class="fa fa-gear text-light-grey"></i> <b>Services 3</b>/5</p>
              </Col>
              <Col sm="2">
                <p className="mb-1"><i class="fa fa-file-o text-light-grey"></i> <b>Counters 4</b>/5</p>
                <p><i class="icon-user text-light-grey"></i> <b>Agents 4</b>/6</p>
              </Col>
              <Col sm="5" className="text-right">
                <span class="badge badge-info pl-3 pr-3 mb-1"><i class="icon-user"></i> Avg. Service Time 30 min</span>
                <br></br>
                <span class="badge badge-info pl-3 pr-3"><i class="fa fa-hourglass-half"></i> Avg. Waiting Time 22 min</span>
              </Col>
            </Row>

            <Card className="mb-0">
              
              <CardBody className="p-0">

              <div className="pt-3 pl-3 pr-3 pb-1">

              <Row>
                <Col sm="6">
                  <h3 className="mb-0">Tickets in Queue . 5</h3>
                  <p className="text-warning mb-0">2 Fast Pass</p>
                </Col>

                <Col sm="6" className="text-right">
                
                  <div className="float-right">
                    <div className="btn-group">
                      <button className="btn btn-outline-secondary w-100 ml-2 mr-2">Sort By <i className="fa fa-sort-amount-asc"></i></button>
                      <select className="form-control">
                        <option>View All</option>
                        <option>Option 1</option>
                        <option>Option 2</option>
                      </select>
                    </div>
                  </div>
                  <div class="input-group float-right w-auto">
                      <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" class="form-control b-r-0 bg-transparent"></input>
                    <div class="input-group-append"><span class="input-group-text bg-transparent b-l-0"><i class="fa fa-search"></i></span></div>
                  </div>
                  <div className="clearfix"></div>

                </Col>
              </Row>
              </div>

                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                    <th className="text-center">#</th>
                    <th className="text-center">Ticket</th>
                    <th className="text-center">Created at</th>
                    <th className="text-center">WT</th>
                    <th className="text-center">Counter</th>
                    <th>Agent</th>
                    <th>Services Type</th>
                    <th className="text-center">Type</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td className="text-center">1</td>
                    <td className="text-center">J-1345</td>
                    <td className="text-center">10:03 AM</td>
                    <td className="text-center">35.03 min</td>
                    <td className="text-center">2</td>
                    <td>
                      k4872<br></br>
                      <h6>Agent Name</h6>
                      </td>
                    <td>doller sit amet conset ipsum.</td>
                    <td className="text-center">
                      <span class="badge badge-info">Standard</span>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">2</td>
                    <td className="text-center">J-1345</td>
                    <td className="text-center">10:03 AM</td>
                    <td className="text-center">35.03 min</td>
                    <td className="text-center">2</td>
                    <td>
                      k4872<br></br>
                      <h6>Agent Name</h6>
                      </td>
                    <td>doller sit amet conset ipsum.</td>
                    <td className="text-center">
                      <span class="badge badge-warning">Fast Pass</span>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">3</td>
                    <td className="text-center">J-1345</td>
                    <td className="text-center">10:03 AM</td>
                    <td className="text-center">35.03 min</td>
                    <td className="text-center">2</td>
                    <td>
                      k4872<br></br>
                      <h6>Agent Name</h6>
                      </td>
                    <td>doller sit amet conset ipsum.</td>
                    <td className="text-center">
                    <span class="badge badge-warning">Fast Pass</span>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">4</td>
                    <td className="text-center">J-1345</td>
                    <td className="text-center">10:03 AM</td>
                    <td className="text-center">35.03 min</td>
                    <td className="text-center">2</td>
                    <td>
                      k4872<br></br>
                      <h6>Agent Name</h6>
                      </td>
                    <td>doller sit amet conset ipsum.</td>
                    <td className="text-center">
                      <span class="badge badge-info">Standard</span>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">5</td>
                    <td className="text-center">J-1345</td>
                    <td className="text-center">10:03 AM</td>
                    <td className="text-center">35.03 min</td>
                    <td className="text-center">2</td>
                    <td>
                      k4872<br></br>
                      <h6>Agent Name</h6>
                      </td>
                    <td>doller sit amet conset ipsum.</td>
                    <td className="text-center">
                      <span class="badge badge-info">Standard</span>
                    </td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                 
                  
                  </tbody>
                </Table>
               
              </CardBody>
            </Card>
          
          

              </TabPane>
              <TabPane tabId="2">
                
              <Row>
              <Col sm="3">
                <h4 className="mb-0">Branch's Name</h4>
                <p><i class="fa fa-map-marker text-light-grey"></i> Location here. <Link>Open Map</Link></p>
              </Col>
              <Col sm="2">
                <p className="mb-1"><i class="fa fa-ticket text-light-grey"></i> <b>Total #Tickets 8</b></p>
                <p><i class="fa fa-gear text-light-grey"></i> <b>Services 3</b>/5</p>
              </Col>
              <Col sm="2">
                <p className="mb-1"><i class="fa fa-file-o text-light-grey"></i> <b>Counters 4</b>/5</p>
                <p><i class="icon-user text-light-grey"></i> <b>Agents 4</b>/6</p>
              </Col>
              <Col sm="5" className="text-right">
                <span class="badge badge-info pl-3 pr-3 mb-1"><i class="icon-user"></i> Avg. Service Time 30 min</span>
                <br></br>
                <span class="badge badge-info pl-3 pr-3"><i class="fa fa-hourglass-half"></i> Avg. Waiting Time 22 min</span>
              </Col>
            </Row>


              </TabPane>
              
            </TabContent>
            </div>




      


      </div>
    );
  }
}

export default Breadcrumbs;
