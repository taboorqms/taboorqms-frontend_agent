import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table, ButtonGroup, ButtonDropdown, DropdownItem,  DropdownMenu,  DropdownToggle,ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';
import { Link } from 'react-router-dom';
import {Line, Pie } from 'react-chartjs-2';
import { AppSwitch } from '@coreui/react'


class Breadcrumbs extends Component {




  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }








  











  render() {
    return (
      <div className="animated fadeIn">
        <Row className="mb-3 hidden">
          
          <Col sm="2">
                <h4 className="mb-0">Branch's Name</h4>
                <p><i class="fa fa-map-marker text-light-grey"></i> Location here. <Link>Open Map</Link></p>
              </Col>
              
              <Col sm="3" className="text-right">
                <span class="badge badge-info pl-3 pr-3 mb-1"><i class="icon-user"></i> Avg. Service Time 30 min</span>
                <br></br>
                <span class="badge badge-info pl-3 pr-3"><i class="fa fa-hourglass-half"></i> Avg. Waiting Time 22 min</span>
              </Col>
          <Col sm="7" className="text-right">
          
            <button className="btn btn-outline-secondary ml-2">Delete <i className="fa fa-trash-o"></i></button>
            <button className="btn btn-outline-secondary ml-2">Edit <i className="fa fa-pencil"></i></button>
            <button className="btn btn-outline-secondary ml-2">More <i className="fa fa-ellipsis-v"></i></button>
           
          </Col>
        </Row>


        <Row className="mb-3">

          <Col xs="12" sm="6" lg="3">
            <Card className="card-simple">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                        <i className="fa fa-calendar"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p>Created on</p>
                      <h5>23 Oct 2019</h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="card-simple">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                        <i className="icon-clock"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p>Average Service Time</p>
                      <h5>22 min</h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="card-simple">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                        <i className="icon-star"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p>Custome Rating</p>
                      <h5>
                        <i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star-o text-light-grey"></i> 4.00</h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="card-simple">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                        <i className="fa fa-ticket"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p>Avg. Number of Tickets</p>
                      <h5>24 Ticket/Day</h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>
        </Row>
        


        <Row>
          <Col sm="8">
          <Card>
              
              <CardBody>
                
                <Row className="mb-3">
                  <Col sm="8">
                      <h4>Available Services . 3</h4>
                  </Col>
                  <Col sm="4" className="text-right">
                      <button className="btn btn-outline-warning ml-2"><i className="fa fa-plus"></i> Add</button>
                  </Col>
                </Row>
                

                <Card className="card-box">
                  <CardHeader>
                    <h5 className="text-primary float-left">Quisque non lectus et nulla aliquet lacinia aget id a augue</h5>
                    <div className="card-header-actions float-right">
                      <Link className="text-light-grey"><i className="fa fa-pencil"></i></Link>
                      <Link className="text-light-grey ml-3"><i className="fa fa-times"></i></Link>
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm">
                      <tr>
                        <td width="35%">Average Waiting Time</td>
                        <td>Counters 4</td>
                    </tr>
                      <tr>
                        <th>30 minutes</th>
                        <th>001 . 002 . 003 . 004</th>
                      </tr>
                  </Table>
                  </CardBody>
                </Card>


                <Card className="card-box">
                  <CardHeader>
                    <h5 className="text-primary float-left">Quisque non lectus et nulla aliquet lacinia aget id a augue</h5>
                    <div className="card-header-actions float-right">
                      <Link className="text-light-grey"><i className="fa fa-pencil"></i></Link>
                      <Link className="text-light-grey ml-3"><i className="fa fa-times"></i></Link>
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm">
                      <tr>
                        <td width="35%">Average Waiting Time</td>
                        <td>Counters 4</td>
                    </tr>
                      <tr>
                        <th>30 minutes</th>
                        <th>001 . 002 . 003 . 004</th>
                      </tr>
                  </Table>
                  </CardBody>
                </Card>


                <Card className="card-box mb-0">
                  <CardHeader>
                    <h5 className="text-primary float-left">Quisque non lectus et nulla aliquet lacinia aget id a augue</h5>
                    <div className="card-header-actions float-right">
                      <Link className="text-light-grey"><i className="fa fa-pencil"></i></Link>
                      <Link className="text-light-grey ml-3"><i className="fa fa-times"></i></Link>
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm">
                      <tr>
                        <td width="35%">Average Waiting Time</td>
                        <td>Counters 4</td>
                    </tr>
                      <tr>
                        <th>30 minutes</th>
                        <th>001 . 002 . 003 . 004</th>
                      </tr>
                  </Table>
                  </CardBody>
                </Card>


              </CardBody>
            </Card>
          
            <Card className="mb-0">
              
              <CardBody>
                
                <Row className="mb-3">
                  <Col sm="8">
                      <h4>Agents . 7</h4>
                  </Col>
                  <Col sm="4" className="text-right">
                      <button className="btn btn-outline-warning ml-2"><i className="fa fa-plus"></i> Add</button>
                  </Col>
                </Row>
                

                <Card className="card-box">
                  <CardHeader>
                    <div className="float-left mr-3">
                      <span class="badge badge-light font-2xl p-2">A</span>
                    </div>
                    <div className="float-left">
                      <p className="text-warning mb-0">K6784</p>
                      <h5 className="text-primary float-left">Agents name here</h5>
                    </div>
                    
                    <div className="card-header-actions float-right">
                      <Link className="text-light-grey"><i className="fa fa-pencil"></i></Link>
                      <Link className="text-light-grey ml-3"><i className="fa fa-times"></i></Link>
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm">
                      <tr>
                        <td>Counters 001 . 005</td>
                    </tr>
                    <tr>
                        <td>Privileges 3</td>
                    </tr>
                  </Table>


                    <span class="badge badge-tag p-2 mr-2">Fusce sodales quis metus eget diagnissim</span>
                    <span class="badge badge-tag p-2 mr-2">Fusce sodales quis metus eget diagnissim</span>
                    <span class="badge badge-tag p-2">Fusce sodales quis metus eget diagnissim</span>

                  </CardBody>
                </Card>

                <Card className="card-box">
                  <CardHeader>
                    <div className="float-left mr-3">
                      <span class="badge badge-light font-2xl p-2">A</span>
                    </div>
                    <div className="float-left">
                      <p className="text-warning mb-0">K6784</p>
                      <h5 className="text-primary float-left">Agents name here</h5>
                    </div>
                    
                    <div className="card-header-actions float-right">
                      <Link className="text-light-grey"><i className="fa fa-pencil"></i></Link>
                      <Link className="text-light-grey ml-3"><i className="fa fa-times"></i></Link>
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm">
                      <tr>
                        <td>Counters 001 . 005</td>
                    </tr>
                    <tr>
                        <td>Privileges 3</td>
                    </tr>
                  </Table>


                    <span class="badge badge-tag p-2 mr-2">Fusce sodales quis metus eget diagnissim</span>
                    <span class="badge badge-tag p-2 mr-2">Fusce sodales quis metus eget diagnissim</span>
                    <span class="badge badge-tag p-2">Fusce sodales quis metus eget diagnissim</span>

                  </CardBody>
                </Card>

                <div className="text-center">
                <Link>View All</Link>
              </div>



              </CardBody>
            </Card>
          

          </Col>
          <Col sm="4">
          


          <Card>
              
              <CardBody>
                
                <Row className="mb-3">
                  <Col sm="8">
                      <h4>Counters . 6</h4>
                  </Col>
                  <Col sm="4" className="text-right">
                      <button className="btn btn-outline-warning ml-2"><i className="fa fa-plus"></i> Add</button>
                  </Col>
                </Row>
                

                <Card className="card-box">
                  <CardHeader>
                    <h5 className="text-primary float-left">001</h5>
                    <div className="card-header-actions float-right">
                      <Link className="text-light-grey"><i className="fa fa-pencil"></i></Link>
                      <Link className="text-light-grey ml-3"><i className="fa fa-times"></i></Link>
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm">
                  <thead>
                  <tr>
                        <td width="35%">Services 3</td>
                        <td>Agents 3</td>
                    </tr>
                    </thead>
                    
                  </Table>
                  </CardBody>
                </Card>

                <Card className="card-box">
                  <CardHeader>
                    <h5 className="text-primary float-left">002</h5>
                    <div className="card-header-actions float-right">
                      <Link className="text-light-grey"><i className="fa fa-pencil"></i></Link>
                      <Link className="text-light-grey ml-3"><i className="fa fa-times"></i></Link>
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm">
                  <thead>
                  <tr>
                        <td width="35%">Services 2</td>
                        <td>Agents 3</td>
                    </tr>
                    </thead>
                    
                  </Table>
                  </CardBody>
                </Card>

                <Card className="card-box">
                  <CardHeader>
                    <h5 className="text-primary float-left">003</h5>
                    <div className="card-header-actions float-right">
                      <Link className="text-light-grey"><i className="fa fa-pencil"></i></Link>
                      <Link className="text-light-grey ml-3"><i className="fa fa-times"></i></Link>
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm">
                  <thead>
                  <tr>
                        <td width="35%">Services 2</td>
                        <td>Agents 3</td>
                    </tr>
                    </thead>
                    
                  </Table>
                  </CardBody>
                </Card>


                <Card className="card-box">
                  <CardHeader>
                    <h5 className="text-primary float-left">004</h5>
                    <div className="card-header-actions float-right">
                      <Link className="text-light-grey"><i className="fa fa-pencil"></i></Link>
                      <Link className="text-light-grey ml-3"><i className="fa fa-times"></i></Link>
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm">
                      <tr>
                        <td width="35%">Services 2</td>
                        <td>Agents 3</td>
                    </tr>
                  </Table>
                  </CardBody>
                </Card>


              <div className="text-center">
                <Link>View All</Link>
              </div>

              </CardBody>
            </Card>
          
            <Card>
              
              <CardBody>
                
                <h4 className="mb-0">Working Hours</h4>
                <p className="text-success">Open Now</p>
                
                <Table responsive className="m-0 table-borderless table-sm">
                  
                    <tbody>
                      <tr>
                        <td width="35%">Sunday</td>
                        <td>09:00 AM - 02:00 PM</td>
                      </tr>
                      <tr>
                        <td className="text-success">Wednesday</td>
                        <td>09:00 AM - 02:00 PM</td>
                      </tr>
                      <tr>
                        <td>Thursday</td>
                        <td>09:00 AM - 02:00 PM</td>
                      </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td className="border-top">Saturday</td>
                          <td className="border-top">09:00 AM - 02:00 PM</td>
                        </tr>
                      </tfoot>
                  </Table>
              </CardBody>
            </Card>
          

          <Card className="mb-0 card-primary">
            
            <CardBody className="text-left">
                <p className="text-white"><i className="fa fa-envelope-o text-warning"></i> info@lorem.com</p>
                <p className="text-white mb-0"><i className="fa fa-phone text-warning"></i> 00971 23 45678923</p>
            </CardBody>
          </Card>
          
          </Col>
        </Row>
            
          


      


      </div>
    );
  }
}

export default Breadcrumbs;
