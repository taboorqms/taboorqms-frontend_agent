import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table, ButtonGroup, ButtonDropdown, DropdownItem,  DropdownMenu,  DropdownToggle, } from 'reactstrap';

import { Link } from 'react-router-dom';

class Breadcrumbs extends Component {




  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }




  render() {
    return (
      <div className="animated fadeIn">
        <Row className="mb-3 hidden">
          <Col sm="6">
            <h3><input type="checkbox"></input> 38 Agents</h3>
          </Col>

          <Col sm="6" className="text-right">
          
            <div className="float-right">
            <button className="btn btn-outline-secondary ml-2">Filters <i className="fa fa-filter"></i></button>
            <button className="btn btn-outline-secondary ml-2">Sort By <i className="fa fa-sort-amount-asc"></i></button>
            <button className="btn btn-outline-warning ml-2"><i class="fa fa-plus"></i></button>
            </div>
            <div class="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" class="form-control b-r-0 bg-transparent"></input>
              <div class="input-group-append"><span class="input-group-text bg-transparent b-l-0"><i class="fa fa-search"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>
        </Row>



            <Card className="mb-0">
              
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                    <th></th>
                    <th className="text-center">ID</th>
                    <th>Name</th>
                    <th className="text-center">
                        <ButtonDropdown id="card1" isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                          <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                          Assig. Counters
                          </DropdownToggle>
                          <DropdownMenu right>
                            <DropdownItem>Action</DropdownItem>
                            <DropdownItem>Another action</DropdownItem>
                            <DropdownItem disabled>Disabled action</DropdownItem>
                            <DropdownItem>Something else here</DropdownItem>
                          </DropdownMenu>
                        </ButtonDropdown>
                    </th>
                    <th>Branch</th>
                    <th className="text-center">
                    <ButtonDropdown id="card2" isOpen={this.state.card2} toggle={() => { this.setState({ card2: !this.state.card2 }); }}>
                          <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                          Privilegs
                          </DropdownToggle>
                          <DropdownMenu right>
                            <DropdownItem>Action</DropdownItem>
                            <DropdownItem>Another action</DropdownItem>
                            <DropdownItem disabled>Disabled action</DropdownItem>
                            <DropdownItem>Something else here</DropdownItem>
                          </DropdownMenu>
                        </ButtonDropdown>
                    </th>
                    <th className="text-center">Avg Handle Time</th>
                    <th className="text-center">Last Login</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    
                    <td className="text-center">K3476</td>
                    <td>Lunna Morrison</td>
                    <td className="text-center">
                        <span class="badge badge-success">001</span>
                    </td>
                    <td><b>Branch's Name</b><br></br><i class="fa fa-map-marker text-light-grey"></i> Location here</td>
                    <td className="text-center">3</td>
                    <td className="text-center">25:00</td>
                    <td className="text-center">10:00 PM <br></br>12.4.19</td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    
                    <td className="text-center">K3476</td>
                    <td>Lunna Morrison</td>
                    <td className="text-center">
                        <span class="badge badge-success">004</span>
                    </td>
                    <td><b>Branch's Name</b><br></br><i class="fa fa-map-marker text-light-grey"></i> Location here</td>
                    <td className="text-center">3</td>
                    <td className="text-center">25:00</td>
                    <td className="text-center">10:00 PM <br></br>12.4.19</td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    
                    <td className="text-center">K3476</td>
                    <td>Lunna Morrison</td>
                    <td className="text-center">
                        <span class="badge badge-success">002</span>
                    </td>
                    <td><b>Branch's Name</b><br></br><i class="fa fa-map-marker text-light-grey"></i> Location here</td>
                    <td className="text-center">3</td>
                    <td className="text-center">25:00</td>
                    <td className="text-center">10:00 PM <br></br>12.4.19</td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    
                    <td className="text-center">K3476</td>
                    <td>Lunna Morrison</td>
                    <td className="text-center">
                        <span class="badge badge-danger">Idle</span>
                    </td>
                    <td><b>Branch's Name</b><br></br><i class="fa fa-map-marker text-light-grey"></i> Location here</td>
                    <td className="text-center">3</td>
                    <td className="text-center">25:00</td>
                    <td className="text-center">10:00 PM <br></br>12.4.19</td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    
                    <td className="text-center">K3476</td>
                    <td>Lunna Morrison</td>
                    <td className="text-center">
                        <span class="badge badge-success">003</span>
                    </td>
                    <td><b>Branch's Name</b><br></br><i class="fa fa-map-marker text-light-grey"></i> Location here</td>
                    <td className="text-center">3</td>
                    <td className="text-center">25:00</td>
                    <td className="text-center">10:00 PM <br></br>12.4.19</td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    
                    <td className="text-center">K3476</td>
                    <td>Lunna Morrison</td>
                    <td className="text-center">
                    <span class="badge badge-danger">Idle</span>
                    </td>
                    <td><b>Branch's Name</b><br></br><i class="fa fa-map-marker text-light-grey"></i> Location here</td>
                    <td className="text-center">3</td>
                    <td className="text-center">25:00</td>
                    <td className="text-center">10:00 PM <br></br>12.4.19</td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    
                    <td className="text-center">K3476</td>
                    <td>Lunna Morrison</td>
                    <td className="text-center">
                    <span class="badge badge-danger">Idle</span>
                    </td>
                    <td><b>Branch's Name</b><br></br><i class="fa fa-map-marker text-light-grey"></i> Location here</td>
                    <td className="text-center">3</td>
                    <td className="text-center">25:00</td>
                    <td className="text-center">10:00 PM <br></br>12.4.19</td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    
                    <td className="text-center">K3476</td>
                    <td>Lunna Morrison</td>
                    <td className="text-center">
                    <span class="badge badge-danger">Idle</span>
                    </td>
                    <td><b>Branch's Name</b><br></br><i class="fa fa-map-marker text-light-grey"></i> Location here</td>
                    <td className="text-center">3</td>
                    <td className="text-center">25:00</td>
                    <td className="text-center">10:00 PM <br></br>12.4.19</td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div class="checkbox form-check">
                          <input id="checkbox1" name="checkbox1" type="checkbox" class="form-check-input"></input>
                          <label for="checkbox1" class="form-check-label form-check-label"></label>
                      </div>
                    </td>
                    
                    <td className="text-center">K3476</td>
                    <td>Lunna Morrison</td>
                    <td className="text-center">
                        <span class="badge badge-success">004</span>
                    </td>
                    <td><b>Branch's Name</b><br></br><i class="fa fa-map-marker text-light-grey"></i> Location here</td>
                    <td className="text-center">3</td>
                    <td className="text-center">25:00</td>
                    <td className="text-center">10:00 PM <br></br>12.4.19</td>
                    <td className="text-center"><Link><i className="fa fa-ellipsis-v f20 text-light-grey"></i></Link></td>
                  </tr>
                  
                  </tbody>
                </Table>
               
              </CardBody>
            </Card>
          
          


      


      </div>
    );
  }
}

export default Breadcrumbs;
