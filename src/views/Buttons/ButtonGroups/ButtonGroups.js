import React, { Component } from "react";
import {
  Card,
  CardBody,
  Alert,
  Col,
  Row,
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";
import { Line } from "react-chartjs-2";
import ls from "local-storage";
const line = {
  labels: [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ],
  datasets: [
    {
      label: "Performance",
      Color: ["rgb(108, 135, 254)"],
      labels: {
        fontColor: "rgb(255, 99, 132)",
      },
      line: {
        borderColor: "#F85F73",
      },
      fill: true,
      lineTension: 0.1,
      backgroundColor: "rgba(255, 255, 255, 0.1)",
      borderColor: "rgb(108, 135, 254)",
      borderCapStyle: "butt",
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: "miter",
      pointBorderColor: "rgb(108, 135, 254)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(255, 255, 255, 0.8)",
      pointHoverBorderColor: "rgba(255, 255, 255, 0.8)",
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [10, 20, 30, 25, 40, 45],
    },
  ],
};
var canvas = document.createElement("canvas");
const ctx = canvas.getContext("2d");
const gradient = ctx.createLinearGradient(0, 0, 500, 0);
gradient.addColorStop(0, "orange");
gradient.addColorStop(0.5, "red");
gradient.addColorStop(1, "green");
ctx.fillStyle = gradient;
ctx.fillRect(20, 20, 150, 100);
class Breadcrumbs extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      agentDetals: [],
      selectedWeek:'1 week',
      agntid:'',
      line: {
        labels: [ "Jan", "Feb", "Mar", "Apr",  "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
        datasets: [
          {
            borderColor: gradient,
            pointRadius: 0,
            fill: false,
            borderWidth: 2,
            hoverBackgroundColor: "rgba(255,99,132,0.4)",
            hoverBorderColor: "rgba(255,99,132,1)",
            data: [],
          },
        ],
      },
    };
  }

  toggle() {
    this.setState({ dropdownOpen: !this.state.dropdownOpen });
  }

  getAgentDetails = async () => {
    fetch(global.url + "/kcatmanagement/agent/getDetails", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + ls.get("token"),
      },
    }).then((response) => response.json()).then(async (res) => {
      this.setState({agentid:res.agentList[0].agent.agentId})
      this.setState({ agentDetals: res.agentList[0] });
      var date = new Date();
      var last = new Date(date.getTime() - (6 * 24 * 60 * 60 * 1000));
      this.getStatictics(last,new Date());
    }).catch((error) => {
      console.log(error);
    });
  };
  
  getStatictics(startDate,endDate){
    fetch(global.url + "/kcatmanagement/agent/getStatistics", {
      method: 'POST',
      body:JSON.stringify({
        "idList": [this.state.agentid],
        "endDate": endDate,
        "startDate": startDate
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "Bearer " + ls.get("token"),
      },
    }).then(response => response.json()).then(responsejson => {
      console.log('==================getStatistics==================');
      console.log(responsejson);
      console.log('==================getStatistics==================');
      if(responsejson.applicationStatusCode==0)
      {
        var localLine={
          labels: [],
          datasets: [
            {
              borderColor: gradient,
              pointRadius: 0,
              fill: false,
              borderWidth: 2,
              hoverBackgroundColor: 'rgba(255,99,132,0.4)',
              hoverBorderColor: 'rgba(255,99,132,1)',
              data: []
            }
          ]
        }
        if(responsejson.values!=null) {
          for(var i=0;i<responsejson.values.length;i++) {
            localLine.labels.push(responsejson.values[i].key);
            localLine.datasets[0].data.push(responsejson.values[i].value);
          }
          this.setState({ line:localLine }) 
        }
      }
    })
    }
  componentDidMount() {
    this.getAgentDetails();
  }

  changeDays=e=>{
    this.setState({selectedWeek: e.currentTarget.textContent})
    {
      if(e.currentTarget.textContent=='1 week')
      {
        var date = new Date();
        var last = new Date(date.getTime() - (6 * 24 * 60 * 60 * 1000))
        this.getStatictics(last,new Date())
      }
      else if(e.currentTarget.textContent=='2 week'){
        var date = new Date();
        var last = new Date(date.getTime() - (13 * 24 * 60 * 60 * 1000))
        this.getStatictics(last,new Date())
      }
      else if(e.currentTarget.textContent=='1 month'){
        var oneMonthAgo = new Date(
          new Date().getFullYear(),
          new Date().getMonth() - 1, 
          new Date().getDate()
      );
        this.getStatictics(oneMonthAgo,new Date())
      }
      else if(e.currentTarget.textContent=='6 month')
      {
        var oneMonthAgo = new Date(
          new Date().getFullYear(),
          new Date().getMonth() - 6, 
          new Date().getDate()
      );
        this.getStatictics(oneMonthAgo,new Date())
      }
    }
    
  }
  render() {
    let { agentDetals } = this.state;
    console.log("=========agenttt", agentDetals.agent);
    return (
      <div className="animated fadeIn">
        {agentDetals.length != 0 ? (
          <>
            <Row>
              <Col lg="6">
                <div className="float-left mr-3">
                  <span class="badge btn btn-primary btn-sm font-2xl font-weight-light pb-2 pl-3 pr-3 bg-box">
                    {agentDetals.agent.serviceCenterEmployee.user.name.charAt(
                      0
                    )}
                  </span>
                </div>
                <div className="float-left">
                  <p className="text-warning mb-0">
                    {" "}
                    {
                      agentDetals.agent.serviceCenterEmployee.employeeNumber
                    }{" "}
                  </p>
                  <h4 className="mb-1">
                    {agentDetals.agent.serviceCenterEmployee.user.name}{" "}
                    <sup className="text-success font-3xl">.</sup>
                    <small>
                      {global.Language == "EN" ? (
                        <span class="badge badge-success ml-2">
                          Counter{" "}
                          {agentDetals.assignedBranchCounter.counterNumber}
                        </span>
                      ) : (
                        <span class="badge badge-success ml-2">
                          الشباك
                          {agentDetals.assignedBranchCounter.counterNumber}
                        </span>
                      )}
                    </small>
                  </h4>
                  {global.Language == "EN" ? (
                    <p className="text-light-grey mb-0">
                      <i className="icon2-calendar text-light-grey"></i> Last
                      Login{" "}
                      {
                        agentDetals.agent.serviceCenterEmployee.user
                          .lastLoginTime
                      }
                    </p>
                  ) : (
                    <p className="text-light-grey mb-0">
                      <i className="icon2-calendar text-light-grey"></i> آخر
                      تسجيل دخول{" "}
                      {
                        agentDetals.agent.serviceCenterEmployee.user
                          .lastLoginTime
                      }
                    </p>
                  )}
                  <p className="text-light-grey">
                    <i className="icon2-email-envelop"></i>{" "}
                    {agentDetals.agent.serviceCenterEmployee.user.email}
                  </p>
                </div>
              </Col>

              {/* <Col sm="6" className="text-right">
                <button
                  className="btn btn-outline-secondary ml-2"
                  onClick={() => this.deleteAgent()}
                >
                  Delete <i className="icon2-trash-remove"></i>
                </button>
                <button
                  className="btn btn-outline-secondary ml-2"
                  onClick={() => this.editAgent()}
                >
                  Edit <i className="icon2-edit-pencil"></i>
                </button>
                <button className="btn btn-outline-secondary ml-2">
                  More <i className="icon2-menu-options font-sm"></i>
                </button>
              </Col> */}
            </Row>

            <Row className="mb-3">
              <Col xs="12" sm="6" lg="3">
                <Card className="card-simple">
                  <CardBody>
                    <div className="row">
                      <div className="col-3">
                        <div className="widget-icon">
                          <i className="icon-branches font-3xl"></i>
                        </div>
                      </div>
                      <div className="col-9">
                        <div className="widget-title">
                          {global.Language == "EN" ? <p>Branch</p> : <p>فرع</p>}
                          <h5> {agentDetals.agent.branch.branchName}</h5>
                        </div>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </Col>

              <Col xs="12" sm="6" lg="3">
                <Card className="card-simple">
                  <CardBody>
                    <div className="row">
                      <div className="col-3">
                        <div className="widget-icon">
                          <i className="icon-clock-time font-3xl"></i>
                        </div>
                      </div>
                      <div className="col-9">
                        <div className="widget-title">
                          {global.Language == "EN" ? (
                            <p>Average Handle Time</p>
                          ) : (
                            <p>متوسط وقت التعامل</p>
                          )}
                          <h5> {agentDetals.agent.averageServiceTime}</h5>
                        </div>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </Col>

              <Col xs="12" sm="6" lg="3">
                <Card className="card-simple">
                  <CardBody>
                    <div className="row">
                      <div className="col-3">
                        <div className="widget-icon">
                          <i className="icon-wall-clock font-3xl"></i>
                        </div>
                      </div>
                      <div className="col-9">
                        <div className="widget-title">
                          {global.Language == "EN" ? (
                            <p>Total Work Time</p>
                          ) : (
                            <p>Tإجمالي وقت العمل</p>
                          )}
                          <h5>{agentDetals.agent.totalWorkHours}</h5>
                        </div>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </Col>

              <Col xs="12" sm="6" lg="3">
                <Card className="card-simple">
                  <CardBody>
                    <div className="row">
                      <div className="col-3">
                        <div className="widget-icon">
                          <i className="icon-recipt font-3xl"></i>
                        </div>
                      </div>
                      <div className="col-9">
                        <div className="widget-title">
                          {global.Language == "EN" ? (
                            <p>Total Tickets Served</p>
                          ) : (
                            <p>إجمالي التذاكر المقدمة</p>
                          )}
                          <h5>{agentDetals.agent.totalTicketServed}</h5>
                        </div>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>

            <Row>
              <Col lg="8">
                <Card>
                  <CardBody className="text-left">
                    <h4 className="float-left">Performance</h4>
                    <ButtonDropdown
                      style={{ float: "right" }}
                      id="card1"
                      isOpen={this.state.card1}
                      toggle={() => {
                        this.setState({ card1: !this.state.card1 });
                      }}
                    >
                      <DropdownToggle
                        caret
                        className="p-0 mb-0 text-light-grey"
                        color="transparent"
                      >
                        {this.state.selectedWeek}
                      </DropdownToggle>
                      <DropdownMenu right>
                        <DropdownItem onClick={this.changeDays}>
                          1 week
                        </DropdownItem>
                        <DropdownItem onClick={this.changeDays}>
                          2 week
                        </DropdownItem>
                        <DropdownItem onClick={this.changeDays}>
                          1 month
                        </DropdownItem>
                        <DropdownItem onClick={this.changeDays}>
                          6 month
                        </DropdownItem>
                      </DropdownMenu>
                    </ButtonDropdown>
                    <div className="clearfix"></div>
                    <div className="chart-wrapper">
                      <Line
                        data={this.state.line}
                        options={{
                          legend: {
                            display: false,
                          },
                          scales: {
                            yAxes: [
                              {
                                ticks: {
                                  fontColor: "white",
                                },
                                gridLines: {
                                  display: true,
                                },
                              },
                            ],
                            xAxes: [
                              {
                                ticks: {
                                  fontColor: "grey",
                                },
                                gridLines: {
                                  display: false,
                                },
                              },
                            ],
                          },
                        }}
                      />
                    </div>
                  </CardBody>
                </Card>
              </Col>
              <Col lg="4">
                <Card>
                  <CardBody>
                    <Row className="mb-3">
                      {global.Language == "EN" ? (
                        <Col sm="8">
                          <h4>Assigned Counters </h4>
                        </Col>
                      ) : (
                        <Col sm="8">
                          <h4>الشبابيك المعينة</h4>
                        </Col>
                      )}
                    </Row>

                    <Alert color="light" className="theme-alert bg-white">
                      {agentDetals.assignedBranchCounter.counterNumber}
                    </Alert>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </>
        ) : null}
      </div>
    );
  }
}
export default Breadcrumbs;
