import React, { Component } from "react";
import { Button, Card, CardBody, Col, Row, Modal, ModalBody, ModalFooter, ModalHeader, } from "reactstrap";
import ls from "local-storage";
import { CircularProgress } from "@material-ui/core";
import { Link } from "react-router-dom";
import Timer from "react-compound-timer";

class Buttons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      ticketData: null,
      showSecondsZero: true,
      showMinutesZero: true,
      showHoursZero: true,
      error: "",
      reset: false,
      counters: [],
      ticketId: null,
      served: 0,
      cancelled: 0,
      trransfered: 0,
      recalled: 0,
      totalSum: 0,
      recalledButton: 0,
      ticketInfo: null,
      disableNext: false,
      disableRecall: false,
      disabletransfer:false,
      disablecancel:false,
      ticketsInQueue: null,
      hideDetails: false,
      // hamza
      queuedata:'',
      queuenumber:0
      // hamza
    };
    this.toggle = this.toggle.bind(this);
  }
  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }
  getServingTicket = async () => {
    fetch(global.url + "/kcatmanagement/agent/serve/ticket", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + ls.get("token"),
      },
    }).then((response) => response.json()).then(async (res) => {
        console.log('===============getServingTicket=====================');
        console.log(res);
        console.log('================getServingTicket====================');
        if (res.applicationStatusCode == 0) {
          this.setState({queuedata:res.ticket,queuenumber:res.ticketsWaitingInQueue})
          this.setState( { ticketData: res.ticket, ticketId: res.ticket.ticketId, ticketInfo: res, hideDetails: false });
          this.getTransferCounters(res.ticket.ticketId);
        } else {
          this.setState({ hideDetails: true});
        }
      });
  };
  getTransferCounters = async (ticketId) => {
    fetch(global.url + "/kcatmanagement/agent/get/transfer/counters", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + ls.get("token"),
      },
      body: JSON.stringify({
        ticketId: ticketId,
      }),
    }).then((response) => response.json()).then(async (res) => {
      console.log('===============getTransferCounters=====================');
      console.log(res);
      console.log('===============getTransferCounters=====================');
      this.setState({ counters: res.counters });
    });
  };
  transferTicket = async (counterId) => {
    let { ticketData } = this.state;
    fetch(global.url + "/kcatmanagement/agent/transfer/ticket", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + ls.get("token"),
      },
      body: JSON.stringify({
        ids: [ticketData.ticketId, counterId]
      }),
    }).then((response) => response.json()).then(async (res) => {
        console.log('===============transferTicket=====================');
        console.log(res);
        console.log('===============transferTicket=====================');
        this.setState({ticketId: null});
        this.getNextTicket();
      })
  };
  getNextTicket = async () => {
    let ticketId = !this.state.hideDetails ? this.state.ticketId : null;
    fetch(global.url + "/kcatmanagement/agent/next/ticket", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + ls.get("token"),
      },
      body: JSON.stringify({
        ticketId: ticketId,
      }),
    }).then((response) => response.json()).then(async (res) => {
        this.getTicketSummary();
        console.log('==============getNextTicket======================');
        console.log(res);
        console.log('==============getNextTicket======================');
        if (res.applicationStatusCode == 0) {
          this.setState(
            { ticketData: res.ticket, ticketId: res.ticket.ticketId, ticketInfo: res, recalledButton: 0, disableRecall: false,
              disableNext: true, showHoursZero: true, showMinutesZero: true, showSecondsZero: true, hideDetails: false,
            }
          );
          this.getTransferCounters(res.ticket.ticketId)
          this.timer.reset();
          setTimeout(() => { this.setState({ disableNext: false})}, 30000);
        } else {
          this.setState({ error: res.devMessage, hideDetails: true});
        }
      })
  };

  cancelTicket = async () => {
    let { ticketData } = this.state;
    if (ticketData !== null) {
      fetch(global.url + "/kcatmanagement/agent/cancel/ticket", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + ls.get("token"),
        },
        body: JSON.stringify({
          ticketId: ticketData.ticketId
        }),
      })
        .then((response) => response.json())
        .then(async (res) => {
          if (res.applicationStatusCode == 0) {
            console.log("resss in cencell", res);
            this.setState(
              {ticketId: null},
              () => this.getNextTicket()
            );
          } else {
            alert(res.devMessage);
          }
        })
        .catch((error) => {
          alert(JSON.stringify(error));
        });
    } else {
      alert("You don't have any ticket");
    }
  };
  recallTicket = async () => {
    let { ticketData } = this.state;
    if (ticketData !== null) {
      fetch(global.url + "/kcatmanagement/agent/recall/ticket", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + ls.get("token"),
        },
        body: JSON.stringify({
          ids: [ticketData.ticketId],
        }),
      })
        .then((response) => response.json())
        .then(async (res) => {
          console.log("resssssssssssssssssss in recallll", this.timer);
          this.timer.reset();
          this.setState(
            {
              reset: true,
              recalledButton: this.state.recalledButton + 1,
              disableRecall: true,
              showHoursZero: true,
              showMinutesZero: true,
              showSecondsZero: true,
            },
            () => this.getTicketSummary()
          );
          setTimeout(() => {
            this.setState({
              disableRecall: false,
            });
          }, 3000);
        })
    } else {
      alert("You don't have any ticket");
    }
  };
  getTicketSummary = () => {
    fetch(global.url + "/kcatmanagement/agent/getTicket/summary", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + ls.get("token"),
      }}).then((response) => response.json()).then(async (res) => {
        this.setState({
          served: res.totalTicketServed,
          cancelled: res.totalTicketCancelled,
          recalled: res.totalTicketRecalled,
          trransfered: res.totalTicketTransferred,
          totalSum: res.totalSum,
        });
      })
  };
  async componentDidMount() {
    this.getServingTicket();
    this.getTicketSummary();
    navigator.serviceWorker.addEventListener("message", (message) => {
      let { ticketsWaitingInQueue } = message.data["firebase-messaging-msg-data"].data;
      this.setState({queuenumber: ticketsWaitingInQueue});
    });
  }
  render() {
    const {
      recalledButton,
      disableRecall,
      disableNext,
      ticketInfo,
      ticketData,
      ticketsInQueue,
      hideDetails,
    } = this.state;
    return (
      <div className="animated fadeIn counter-page">
        <div className="text-center">
          <h5 className="text-primary font-2xl">
            {global.Language == "EN" ? "In Queue."+this.state.queuenumber : "في قائمة الانتظار"+this.state.queuenumber}
          </h5>
          {(ticketData) && (
            <h4 className="text-primary"> {global.Language == "EN"?'Now Serving':'في خدمة'} </h4>
          )}
        </div>
        <Row className="mb-5 mt-5">
          <Col lg="4">
            <Button disabled={ disableNext || hideDetails } onClick={this.cancelTicket} color="danger" block className="btn-style mb-5" >
              {global.Language == "EN" ?'Cancel': 'الغاء'}
              <i className="icon-cancel"></i>
            </Button>
              <Button disabled={ hideDetails || disableRecall || recalledButton > 2 ? true : false}
                onClick={this.recallTicket} color="primary" color={disableRecall || recalledButton > 2 ? "grey" : "primary"} block className="btn-style" >
                {global.Language == "EN" ?'Recall': 'إعادة نداء'}
                 <i className="icon-loading"></i>
              </Button>
          </Col>
          <Col lg="4" className="text-center">
            {!hideDetails ? (
              ticketData ? (
                <>
                  <h1 className="mb-5">{ticketData.ticketNumber}</h1>
                  {(ticketInfo.customerName!="" || ticketInfo.customerEmail!="" || ticketInfo.customerContact!="") && (
                    <h4 className="text-primary">
                      {global.Language == "EN" ?'Customer Details': 'تفاصيل العميل'}
                    </h4>
                  )}
                  <div className="customer-details mb-5 pb-3" style={{ alignItems: "center" }} >
                    <div style={{ color: "#191919" }}>
                      {ticketInfo.customerName}
                    </div>
                    <div style={{ color: "#191919" }}>
                      {ticketInfo.customerEmail}
                    </div>
                    <div style={{ color: "#191919" }}>
                      {ticketInfo.customerContact}
                    </div>
                  </div>
                </>
              ) : (
                <CircularProgress />
              )
            ) : null}
            {(!hideDetails) && (
              <h2 className="font-5xl">
                <Timer ref={(ref) => { this.timer = ref;}}
                  formatValue={(value) => `${value < 10 ? `0${value}` : value}`}
                  checkpoints={[
                    {time: 1000,callback: () => console.log("again 1 secondd"),},
                    {time: 60 * 1000,callback: () => this.setState({ showMinutesZero: true }),},
                    {time: 60 * 1000 * 60,callback: () => this.setState({ showHoursZero: true }),},
                    {time: 1000 * 10,callback: () =>this.setState({showSecondsZero: false,}),},
                    {time: 60 * 1000 * 10,callback: () => this.setState({ showMinutesZero: false }),},
                    {time: 60 * 1000 * 60 * 10,callback: () => this.setState({ showHoursZero: false }),},
                  ]}
                >
                {() => (
                  <>
                    <Timer.Hours />:
                    <Timer.Minutes />:
                    <Timer.Seconds />
                  </>
                )}
                </Timer>
              </h2>
            )}
          </Col>
          <Col lg="4">
            <Button disabled={ disableNext} onClick={this.getNextTicket} color={disableNext ? "grey" : "success"} block className="btn-style mb-5">
              {global.Language == "EN" ?'Next': 'التالي'} <i className="icon-fast-forward"></i>
            </Button>
            <Button disabled={ hideDetails} color="warning" block className="btn-style" onClick={this.toggle} >
              {global.Language == "EN" ?'Transfer':'نقل'}<i className="icon-reply-all"></i>
            </Button>
          </Col>
        </Row>

        <Card className="card-box bg-transparent mt-5">
          <CardBody className="p-3 f20 no-wrap">
            <Row>
              <Col sm="12" md="6" lg="4" xl="3" className="pb-sm-2 font-weight-bold" >
                {global.Language == "EN" ? '# Tickets Served .':'# التذاكر المقدمة.'}
                  {this.state.totalSum}
              </Col>
              <Col sm="12" md="6" lg="4" xl="2" className="text-danger pb-sm-2">
                <i className="icon-cancel"></i>
                {global.Language == "EN" ? 'Canceled .':'ملغية .'}
                {this.state.cancelled}
              </Col>
              <Col sm="12" md="6" lg="4" xl="2" className="text-primary pb-sm-2" >
                <i className="icon-loading"></i>
                {global.Language == "EN" ? 'Recalled .':'منادية .'}
                {this.state.recalled}
              </Col>
              <Col sm="12" md="6" lg="4" xl="2" className="text-warning pb-sm-2" >
                <i className="icon-reply-all"></i>
                {global.Language == "EN" ? 'Transfered .':'منقولة .'}
                {this.state.trransfered}
              </Col>
              <Col sm="12" md="6" lg="4" xl="3" className="text-success pb-sm-2" >
                <i className="icon-check-tick"></i>
                {global.Language == "EN" ? 'Served .':'مقدمة . .'}
                {this.state.served}
              </Col>
            </Row>
          </CardBody>
        </Card>

        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} >
          <ModalHeader className="text-center">
            <i className="icon-reply-all"></i>
            {global.Language == "EN" ?'Transfer Ticket To':'نقل التذكرة إلى'}
          </ModalHeader>
          <ModalBody>
            {this.state.counters.length !== 0 ? (
              this.state.counters.map((item) => (
                <ul className="list-selection">
                  <li>
                    <Link
                      onClick={() => this.transferTicket(item.counterId)}
                    >
                      {item.counterNumber}
                    </Link>
                  </li>
                </ul>
              ))
            ) : (
              <ul className="list-selection">
                <li>
                  <Link disabled>No Counters Available</Link>
                </li>
              </ul>
            )}
          </ModalBody>
          <ModalFooter className="text-center">
            <Button color="link" onClick={this.toggle}>
              {global.Language == "EN" ?'Cancel':'الغاء'}
            </Button>
            <Button color="link" onClick={this.toggle}>
              {global.Language == "EN" ?'Done':'تم'}
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default Buttons;
