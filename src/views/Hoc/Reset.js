import React, { Component } from "react";

export default function Reset(HocComponent, reset) {
  return class extends Component {
    constructor(props) {
      super(props);
      //   this.state = {
      //     data: data,
      //   };
    }

    render() {
      return <HocComponent reset={reset} {...this.props} />;
    }
  };
}
