import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table, ButtonGroup, ButtonDropdown, DropdownItem,  DropdownMenu,  DropdownToggle,ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';
import { Link } from 'react-router-dom';
import {Line, Pie } from 'react-chartjs-2';
import { AppSwitch } from '@coreui/react'

const line = {
  labels: ['May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
  datasets: [
    {
      label: 'Overall Performance',
      Color: [
        'rgb(108, 135, 254)'
      ],
      labels: {
        fontColor: "rgb(255, 99, 132)"
      },
      line: {
        borderColor: '#F85F73',
       },
      fill: true,
      lineTension: 0.1,
      backgroundColor: 'rgba(255, 255, 255, 0.1)',
      borderColor: 'rgb(108, 135, 254)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgb(108, 135, 254)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(255, 255, 255, 0.8)',
      pointHoverBorderColor: 'rgba(255, 255, 255, 0.8)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [10, 20, 30, 25, 40, 45],
    },
  ],
};


class Breadcrumbs extends Component {




  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }








  











  render() {
    return (
      <div className="animated fadeIn">
        <Row className="mb-3 hidden">
          <Col sm="4">
            <div className="float-left mr-3">
              <span class="badge bg-white text-primary font-2xl p-3">Logo</span>
            </div>
            <div className="float-left">
            <h4 className="mb-0">Service Center Name</h4>
                <p><i class="fa fa-map-marker text-light-grey"></i> Location here. <Link>Open Map</Link></p>
            </div>
            <div className="clearfix"></div>
          </Col>
          <Col sm="2">
            <p className="text-light-grey mb-0">Created On</p>
            <h5>02.05.2019</h5>
          </Col>
          <Col sm="2">
            <p className="text-light-grey mb-0">Category</p>
            <h5>Service Type</h5>
          </Col>
          <Col sm="4" className="text-right">
          
            <button className="btn btn-outline-secondary ml-2">Delete <i className="fa fa-trash-o"></i></button>
            <button className="btn btn-outline-secondary ml-2">Edit <i className="fa fa-pencil"></i></button>
            <button className="btn btn-outline-secondary ml-2">More <i className="fa fa-ellipsis-v"></i></button>
           
          </Col>
        </Row>


        <Row className="mb-3">

          <Col xs="12" sm="6" lg="3">
            <Card className="card-simple">
              <CardBody>
                    <div className="ml-2">
                        <h4 className="text-warning">24</h4>
                    </div>
                    <div className="widget-title">
                      <p><i className="icon-present"></i> Branch</p>
                    </div>
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="card-simple">
              <CardBody>
                    <div className="ml-2">
                        <h4 className="text-warning">5</h4>
                    </div>
                    <div className="widget-title">
                      <p><i className="icon-present"></i> Cities</p>
                    </div>
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Card className="card-simple">
              <CardBody>
                    <div className="ml-2">
                        <h4 className="text-warning">38</h4>
                    </div>
                    <div className="widget-title">
                      <p><i className="icon-user"></i> Agents</p>
                    </div>
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="card-simple">
              <CardBody>
                    <div className="ml-2">
                        <h4 className="text-warning">57</h4>
                    </div>
                    <div className="widget-title">
                      <p><i className="icon-user"></i> Users</p>
                    </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        


        <Row>
          <Col sm="8">
          <Card className="mb-0">
              
              <CardBody>
                <Row>
                  <Col sm="8">
                      <h4>Services . 3</h4>
                  </Col>
                  <Col sm="4" className="text-right">
                      <button className="btn btn-outline-warning ml-2"><i className="fa fa-pencil"></i> Edit</button>
                  </Col>
                </Row>
                

                <ul className="list-dots">
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet Lorem ipsum doller sit amet </a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       <li><a>Lorem ipsum doller sit amet</a></li>
                       
                     </ul>

              </CardBody>
            </Card>
          
          </Col>
          <Col sm="4">
          


            <Card>
            
            <CardBody className="text-left">
              <h4 className="text-light-grey">1716 <small>Votes</small></h4>
              <div className="chart-wrapper line-graph" >
                <Line data={line}  />
              </div>
            </CardBody>
          </Card>

          <Card className="mb-0 card-primary">
            
            <CardBody className="text-left">
                <p className="text-white"><i className="fa fa-envelope-o text-warning"></i> info@lorem.com</p>
                <p className="text-white mb-0"><i className="fa fa-phone text-warning"></i> 00971 23 45678923</p>
            </CardBody>
          </Card>
          
          </Col>
        </Row>
            
          


      


      </div>
    );
  }
}

export default Breadcrumbs;
