import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import { AppNavbarBrand } from "@coreui/react";
import logo from "../../../assets/img/brand/logo-color.png";
import { MenuItem, Select, InputLabel, FormGroup } from "@material-ui/core";
import ls from "local-storage";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      counterValue: 0,
      branchList: [],
      counterList: [],
      error: "",
      counterError: "",
      Language: "EN",
    };
  }
  handleChange = (event) => {
    this.setState({
      counterValue: 0,
    });
    this.state.branchList.map((item) => {
      if (item.branchId == event.target.value) {
        let temparr = item.counterList;
        temparr.map((counter, index) => {
          if (counter.setup == true) {
            let tempObj = temparr[0];
            temparr[index] = tempObj;
            temparr[0] = counter;
            this.setState({
              counterValue: temparr[0].counterId,
            });
          }
        });
        this.setState({
          counterList: temparr,
        });
      }
    });
    this.setState({
      value: event.target.value,
    });
  };
  handleCounterChange = (event) => {
    this.setState({
      counterValue: event.target.value,
    });
  };
  getBranches = async () => {
    fetch(global.url + "/kcatmanagement/admin/get/branches", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await ls.get("token")),
      },
    })
      .then((response) => response.json())
      .then(async (res) => {
        console.log('====================================');
        console.log(res);
        console.log('====================================');
        let temparr = res;
        res.map((item, index) => {
          let tempCounterArr = item.counterList;
          item.counterList.map((counter, counterIndex) => {
            if (counter.setup == true) {
              let tempObj = temparr[0];

              temparr[index] = tempObj;
              temparr[0] = item;
              console.log("==========temparr setup =", temparr);

              let tempCounterObj = tempCounterArr[0];

              tempCounterArr[counterIndex] = tempCounterObj;
              tempCounterArr[0] = counter;
              this.setState({
                value: temparr[0].branchId,
                counterList: tempCounterArr,
                counterValue: tempCounterArr[0].counterId,
              });
            }
          });
        });

        this.setState({ branchList: res });
      })
      .catch((error) => {
        // alert(JSON.stringify(error));
      });
  };
  componentDidMount() {
    this.getBranches();
  }
  handleSubmit = async () => {
    if (this.state.value == 0) {
      this.setState({
        error: "*Required",
      });
      return null;
    } else {
      this.setState({
        error: "",
      });
    }
    if (this.state.counterValue == 0) {
      this.setState({
        counterError: "*Required",
      });
      return null;
    } else {
      this.setState({
        counterError: "",
      });
    }

    fetch(global.url + "/kcatmanagement/agent/add/config", {
      method: "POST",

      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + (await ls.get("token")),
      },
      body: JSON.stringify({
        deviceToken: await ls.get("deviceToken"),
        branchCounterId: this.state.counterValue,
      }),
    })
      .then((response) => response.json())
      .then((res) => {
        console.log(
          "ress==================================================================",
          res
        );
        if (res.applicationStatusCode == 0) {
          let { history } = this.props;
          history.push({
            pathname: `/`,
          });
        } else {
          this.setState({
            error: res.devMessage,
          });
        }
        // this.getTicketNumber();
      })
      .catch((error) => {
        // alert(JSON.stringify(error));
      });
  };
  handleLanguageChange = (e) => {
    this.setState({
      Language: e.target.value,
    });
    localStorage.setItem('Language', e.target.value);
    global.Language = e.target.value;
  };
  render() {
    const { value } = this.state;
    return (
      <div className="app flex-row align-items-center login-page">
        <Container>
          <div className="language-selector">
            <select
              className="form-control"
              onChange={(e) => this.handleLanguageChange(e)}
            >
              {" "}
              <option>Select Language</option>
              <option value="AR">العربية</option>
              <option value="EN">English</option>
            </select>
          </div>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup className="card bg-primary">
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <div className="login-logo mb-5 text-center">
                        <AppNavbarBrand
                          full={{ src: logo, width: 250, alt: "Taboor Logo" }}
                        />
                      </div>
                      <div className="form-group">
                        <InputGroup className="input-line">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-counter"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          {/* <Input type="text" placeholder="Counter ID" autoComplete="username" /> */}

                          <Select
                            style={{ flex: 1 }}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={this.state.value}
                            onChange={this.handleChange}
                          >
                            {global.Language == "EN" ? (
                              <MenuItem value={0} disabled>
                                Please Select Branch
                              </MenuItem>
                            ) : (
                              <MenuItem value={0} disabled>
                                اسم الفرع
                              </MenuItem>
                            )}
                            {this.state.branchList.length !== 0
                              ? this.state.branchList.map((item, itemIndex) => (
                                  <MenuItem value={item.branchId}>
                                    {item.branchName}
                                  </MenuItem>
                                ))
                              : null}
                          </Select>
                        </InputGroup>
                        {this.state.error !== "" ? (
                          <span style={{ color: "red", marginTop: 5 }}>
                            {this.state.error}
                          </span>
                        ) : null}

                        {/* // counter */}
                      </div>

                      <div className="form-group">
                        <InputGroup className="input-line">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-counter"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          {/* <Input type="text" placeholder="Counter ID" autoComplete="username" /> */}

                          <Select
                            style={{ flex: 1 }}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={this.state.counterValue}
                            onChange={this.handleCounterChange}
                          >
                            {global.Language == "EN" ? (
                              <MenuItem value={0} disabled>
                                Please Select Counter
                              </MenuItem>
                            ) : (
                              <MenuItem value={0} disabled>
                                شباك رقم{" "}
                              </MenuItem>
                            )}
                            {this.state.counterList.length !== 0
                              ? this.state.counterList.map((item) => (
                                  <MenuItem value={item.counterId}>
                                    {item.counterNumber}
                                  </MenuItem>
                                ))
                              : null}
                          </Select>
                        </InputGroup>
                        {this.state.counterError !== "" ? (
                          <span style={{ color: "red", marginTop: 5 }}>
                            {this.state.counterError}
                          </span>
                        ) : null}
                      </div>

                      <div className="text-center">
                        {/* <Link to="/home"> */}
                        {global.Language == "EN" ? (
                          <Button
                            style={{ marginTop: 16 }}
                            color="primary"
                            className="px-4 btn-lg"
                            onClick={this.handleSubmit}
                          >
                            Enter
                          </Button>
                        ) : (
                          <Button
                            style={{ marginTop: 16 }}
                            color="primary"
                            className="px-4 btn-lg"
                            onClick={this.handleSubmit}
                          >
                            ادخال
                          </Button>
                        )}
                        {/* </Link> */}
                      </div>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
          <div className="text-right setup-btn">
            <Button color="warning" className="btn-lg">
              Setup <i className="fa fa-gear"></i>
            </Button>
          </div>
          <div className="copyright">Powered by TaboorQMS</div>
        </Container>
      </div>
    );
  }
}

export default Login;
