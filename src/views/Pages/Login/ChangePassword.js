import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  FormGroup,
} from "reactstrap";
import { AppNavbarBrand } from "@coreui/react";
import logo from "../../../assets/img/brand/logo-color.png";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import ls from "local-storage";
class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counterNumber: "",
      branchId: "",
      Language: "EN",
            agentData: {},
    };
  }
  handleLogin = async (values) => {
    // this.setState({
    //   loading: true,
    // });
    const url = global.url + "/kcatmanagement/agent/update";
    const body = {
      deviceToken: global.deviceToken,
      userName: values.email,
      password: values.password,
      deviceCode: 5,
      ipAddress: "10.10.10.10",
    };
    axios
      .post(url, body)
      .then(async (response) => {
        // this.setState({
        //   loading: false,
        // });
        console.log("ressss", response.data);
        if (response.data.applicationStatusCode == 0) {
          await ls.set("token", response.data.sessionToken);
          let { history } = this.props;
          history.push({
            pathname: `/login`,
          });

        } else {
          alert(response.data.devMessage, [{ text: "OK" }], {
            cancelable: false,
          });
        }
      })
      .catch((err) => {
        this.setState({
          loading: false,
        });
        console.log("error", err);
      });
  };
  getProfile = async () => {
    fetch(global.url + "/kcatmanagement/agent/get/profile", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + ls.get("token"),
      },
    })
      .then((response) => response.json())
      .then(async (res) => {
        
        console.log("resssssssssssssssssss", res);

        this.setState({ agentData: res });
      })
      .catch((error) => {
        // alert(JSON.stringify(error));
      });
  };
  getCounterConfigInfo = async () => {
    fetch(global.url + "/kcatmanagement/agent/check/config", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // Authorization: "Bearer " + ls.get("token"),
      },
      body: JSON.stringify({
        deviceToken: ls.get("deviceToken"),
      }),
    })
      .then((response) => response.json())
      .then(async (res) => {
        console.log("resssssssssssssssssss counbter config infooo", res);

        this.setState({
          counterNumber: res.counterNumber,
          branchId: res.branchId,
        });
      })
      .catch((error) => {
        // alert(JSON.stringify(error));
      });
  };
  handleChange = (e) => {
    this.setState({
      Language: e.target.value,
    });
    localStorage.setItem('Language', e.target.value);
    global.Language = e.target.value;
  };
  componentDidMount() {
    this.getCounterConfigInfo();
  }
  render() {
    console.log("gloval languagee", global.Language);
    return (
      <div className="app flex-row align-items-center login-page">
        <Container>
          <div className="language-selector">
            <select
              className="form-control"
              onChange={(e) => this.handleChange(e)}
            >
              <option>Select Language</option>
              <option value="AR">العربية</option>
              <option value="EN">English</option>
            </select>
          </div>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup className="card bg-primary">
                <Card className="p-4">
                  <CardBody>
                    {this.state.counterNumber == "" ? (
                      <div className="side-logo text-center mb-2">
                        <AppNavbarBrand
                          full={{ src: logo, width: 250, alt: "Taboor Logo" }}
                        />
                      </div>
                    ) : null}

                    <Formik
                      initialValues={{ email: "", password: "" }}
                      validationSchema={Yup.object({
                        password: Yup.string()
                          .max(20, "Must be 20 characters or less")
                          .min(4, "Must be 4 characters or more")
                          .required("Required"),
                        email: Yup.string().required("Required"),
                      })}
                      onSubmit={(values) => this.handleLogin(values)}
                    >
                      {(formik) => (
                        <Form onSubmit={formik.handleSubmit}>
                          {global.Language == "EN" ? (
                            <h3 className="text-primary mb-5 text-center">
                              Counter ID{" "}
                              {this.state.counterNumber !== ""
                                ? // this.state.branchId +
                                  // " : " +
                                  this.state.counterNumber
                                : null}
                            </h3>
                          ) : (
                            <h3 className="text-primary mb-5 text-center">
                              الشباك
                              {this.state.counterNumber !== ""
                                ? // this.state.branchId +
                                  // " : " +
                                  this.state.counterNumber
                                : null}
                            </h3>
                          )}
                          <FormGroup>
                            <InputGroup className="input-line">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="icon-username"></i>
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                type="text"
                                placeholder={
                                  global.Language == "EN"
                                    ? "Agent ID"
                                    : "رمز العميل"
                                }
                                autoComplete="username"
                                id="email"
                                // {...formik.getFieldProps("email")}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.name}
                              />
                            </InputGroup>
                            {formik.touched.email && formik.errors.email ? (
                              <div>
                                <span style={{ color: "red", marginTop: 5 }}>
                                  {formik.errors.email}
                                </span>
                              </div>
                            ) : null}
                          </FormGroup>
                          <FormGroup>
                            <InputGroup className="input-line">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="icon-password"></i>
                                </InputGroupText>
                              </InputGroupAddon>

                              <Input
                                type="password"
                                placeholder={
                                  global.Language == "EN"
                                    ? "Password"
                                    : "كلمة المرور"
                                }
                                autoComplete="current-password"
                                id="password"
                                {...formik.getFieldProps("password")}
                              />
                            </InputGroup>
                            {formik.touched.password &&
                            formik.errors.password ? (
                              <span style={{ color: "red", marginTop: 5 }}>
                                {formik.errors.password}
                              </span>
                            ) : null}
                          </FormGroup>
                          <div className="text-center">
                            {global.Language == "EN" ? (
                              <Button
                                style={{ marginTop: 16 }}
                                type="submit"
                                color="primary"
                                className="px-4 btn-lg"
                              >
                                ChangePassword
                              </Button>
                            ) : (
                              <Button
                                style={{ marginTop: 16 }}
                                type="submit"
                                color="primary"
                                className="px-4 btn-lg"
                              >
                                تسجيل الدخول
                              </Button>
                            )}
                          </div>
                        </Form>
                      )}
                    </Formik>

                    {/* <Formik
                      initialValues={{ name: "jared" }}
                      onSubmit={(values, actions) => {
                        setTimeout(() => {
                          alert(JSON.stringify(values, null, 2));
                          actions.setSubmitting(false);
                        }, 1000);
                      }}
                    >
                      {(props) => (
                        <form onSubmit={props.handleSubmit}>
                          <input
                            type="text"
                            onChange={props.handleChange}
                            onBlur={props.handleBlur}
                            value={props.values.name}
                            name="name"
                          />
                          {props.errors.name && (
                            <div id="feedback">{props.errors.name}</div>
                          )}
                          <button type="submit">Submit</button>
                        </form>
                      )}
                    </Formik> */}
                    {/* <Form>
                    
                      <h3 className="text-primary mb-5">Counter ID 71037</h3>
                      <InputGroup className="mb-3 input-line">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Agent ID" autoComplete="username" />
                      </InputGroup>
                      <InputGroup className="mb-5 input-line">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="current-password" />
                      </InputGroup>
                      <div className="text-center">
                        <Link to="/dashboard">
                            <Button color="primary" className="px-4">Login</Button>
                          </Link>
                        </div>
                      
                    </Form> */}
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
          <div className="text-right setup-btn">
            <Button color="warning" className="btn-lg">
              Setup <i className="fa fa-gear"></i>
            </Button>
          </div>
          <div className="copyright">Powered by TaboorQMS</div>
        </Container>
      </div>
    );
  }
}

export default ChangePassword;
