import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Table, Label, FormGroup, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { AppNavbarBrand } from '@coreui/react';
import logo from '../../../assets/img/brand/logo.png';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';
import Steps, { Step } from 'rc-steps';



class Register extends Component {
  constructor(props){
    super(props);
    this.state={
      next:0
    }
  }
  nextStep=(next)=>{
    this.setState({next:next});
  }
  render() {
    return (
      <div className="app align-items-center register-page">
        <Container fluid className="p-0">
        <Row>
          
          <Col sm="9">
          {(this.state.next==0) && (
            <Card className="register-card m-0" style={{height:'100vh'}}>
              <CardBody className="p-4"> <Form>
                <Row className="mb-3">
                  <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                  <Col sm="6" className="text-right">  
                    <h6 className="text-light-grey">Already have an account? <Link to="/login">Login Here</Link></h6> 
                  </Col>
                </Row> 


                <h4 className="text-dark mb-3">Basic Information</h4>
                <Row> <Col sm="7"> 
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Center Name</Label>
                    <Input type="text" placeholder="Service Center Name" autoComplete="username" />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Service Type / Sector</Label>
                    <select className="form-control">
                      <option>Banking</option>
                      <option>Option 2</option>
                    </select>
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Country</Label>
                    <Input type="text" placeholder="Country" autoComplete="username" />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Phone</Label>
                    <Input type="text" placeholder="Phone" autoComplete="username" />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Email</Label>
                    <Input type="email" placeholder="abcd@center.ae" autoComplete="username" />
                  </FormGroup> </Col>
                  <Col sm="5" className="text-center">
                    <div className="upload-image">
                      <div class="custom-file-upload">
                          <label for="file-upload" class="custom-file-content"> <i class="icon-arrow-up-circle"></i> <br></br>Upload Logo </label>
                          <input id="file-upload" type="file"/>
                      </div>
                    </div>
                  </Col>
                </Row> 
              
              
              </Form> </CardBody>
              <CardFooter className="p-4 text-right b-a-0">
                <Button color="primary" onClick={()=>{this.nextStep(1)}}>Next</Button>
              </CardFooter>
            </Card>
             )}
          

          {(this.state.next==1) && (
            <Card className="register-card m-0">
              <CardBody className="p-4"> <Form>
                <Row className="mb-3">
                  <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                  <Col sm="6" className="text-right">  
                    <h6 className="text-light-grey">Already have an account? <Link to="/login">Login Here</Link></h6> 
                  </Col>
                </Row> 
                <h4 className="text-dark mb-3">Choose A Subscription Plan</h4>
                
                <Card className="active-plan">
                  <CardBody className="p-4">
                    <Table className="m-0 table-borderless table-sm">
                      <tr>
                        <td rowSpan="4">
                          <h4 className="mb-4"><i className="fa fa-diamond text-warning"></i> Premium Plan Name</h4>
                          <h2>200K $</h2>
                          <h6 className="text-warning">Per Year</h6>
                        </td>
                        <td>
                          Demo
                        </td>
                        <td>#Users</td>
                        <td>#Branches</td>
                      </tr>
                      <tr>
                        <th>1 free month</th>
                        <th>Unlimited</th>
                        <th>Unlimited</th>
                      </tr>
                      <tr> 
                        <td>
                          Support
                        </td>
                        <td>Payment</td>
                        <td></td>
                      </tr>
                      <tr>
                        <th>1 year. <Link>Detail</Link></th>
                        <th>3 installments. <Link>Detail</Link></th>
                      </tr>
                     
                    </Table>
                  </CardBody>
              </Card>

              <Card>
                  <CardBody className="p-4">
                    <Table className="m-0 table-borderless table-sm">
                      <tr>
                        <td rowSpan="4">
                          <h4 className="mb-4"><i className="fa fa-diamond text-primary"></i> Premium Plan Name</h4>
                          <h2>200K $</h2>
                          <h6 className="text-warning">Per Year</h6>
                        </td>
                        <td>
                          Demo
                        </td>
                        <td>#Users</td>
                        <td>#Branches</td>
                      </tr>
                      <tr>
                        <th>1 free month</th>
                        <th>Unlimited</th>
                        <th>Unlimited</th>
                      </tr>
                      <tr> 
                        <td>
                          Support
                        </td>
                        <td>Payment</td>
                        <td></td>
                      </tr>
                      <tr>
                        <th>1 year. <Link>Detail</Link></th>
                        <th>3 installments. <Link>Detail</Link></th>
                      </tr>
                     
                    </Table>
                  </CardBody>
              </Card>


              <Card className="m-0">
                  <CardBody className="p-4">
                    <Table className="m-0 table-borderless table-sm">
                      <tr>
                        <td rowSpan="4">
                          <h4 className="mb-4"><i className="fa fa-diamond text-danger"></i> Premium Plan Name</h4>
                          <h2>200K $</h2>
                          <h6 className="text-warning">Per Year</h6>
                        </td>
                        <td>
                          Demo
                        </td>
                        <td>#Users</td>
                        <td>#Branches</td>
                      </tr>
                      <tr>
                        <th>1 free month</th>
                        <th>Unlimited</th>
                        <th>Unlimited</th>
                      </tr>
                      <tr> 
                        <td>
                          Support
                        </td>
                        <td>Payment</td>
                        <td></td>
                      </tr>
                      <tr>
                        <th>1 year. <Link>Detail</Link></th>
                        <th>3 installments. <Link>Detail</Link></th>
                      </tr>
                     
                    </Table>
                  </CardBody>
              </Card>


              </Form> </CardBody>
              <CardFooter className="p-4 text-right b-a-0">
                <Button outline color="primary" onClick={()=>{this.nextStep(0)}}>Back</Button>
                <Button color="primary" className="ml-2" onClick={()=>{this.nextStep(2)}}>Next</Button>
              </CardFooter>
            </Card>
          )}
          

          {(this.state.next==2) && (
            <Card className="register-card m-0">
              <CardBody className="p-4"> <Form>
                <Row className="mb-3">
                  <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                  <Col sm="6" className="text-right">  
                    <h6 className="text-light-grey">Already have an account? <Link to="/login">Login Here</Link></h6> 
                  </Col>
                </Row> 
                <h4 className="text-dark mb-3">Payments Method</h4>
                <Row> <Col sm="7"> 
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Card Number</Label>
                    <Input type="text" placeholder="Service Center Name" autoComplete="username" />
                  </FormGroup>

                  <Row>
                    <Col sm="6">
                      <FormGroup className="input-line">
                        <Label className="text-light-grey">Expiry Date</Label>
                        <select className="form-control">
                          <option>Banking</option>
                          <option>Option 2</option>
                        </select>
                      </FormGroup>
                  </Col>
                  <Col sm="6">
                    <FormGroup className="input-line">
                      <Label className="text-light-grey">CVV</Label>
                      <Input type="text" placeholder="Country" autoComplete="username" />
                    </FormGroup>
                    </Col>
                  </Row>

                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Cardholder's Name</Label>
                    <Input type="text" placeholder="Phone" autoComplete="username" />
                  </FormGroup>
                  
                  <Row>
                    <Col sm="6">
                      <FormGroup className="input-line">
                        <Label className="text-light-grey">Payment</Label>
                        <select className="form-control">
                          <option>Banking</option>
                          <option>Option 2</option>
                        </select>
                      </FormGroup>
                  </Col>
                  <Col sm="6">
                      <h6 className="text-light-grey m-4">Amount <span className="text-success">80,000 $</span></h6>
                    </Col>
                  </Row>


                    <Card className="card-simple mb-4">
                      <CardBody>
                        <Table className="m-0 table-borderless table-sm">
                          <tr>
                            <td className="text-success">1st Payment<br></br>
                              <small className="text-light-grey">05 Feb 2020</small>
                            </td>
                            <td className="text-success">80,000 $</td>
                          </tr>
                          <tr>
                            <td>2nd Payment<br></br>
                              <small className="text-light-grey">05 May 2020</small>
                            </td>
                            <td>60,000 $</td>
                          </tr>
                          <tr>
                            <td>Final Payment<br></br>
                              <small className="text-light-grey">05 Sep 2020</small>
                            </td>
                            <td>60,000 $</td>
                          </tr>
                          <tr>
                            <td className="text-primary"><b>Total</b></td>
                            <td className="text-primary"><b>200,000 $</b></td>
                          </tr>
                        </Table>
                      </CardBody>
                    </Card>

                    <p className="text-light-grey">I confirm I have read and accepted the <Link>Terms and Conditions of Use</Link> By clicking on "<span className="text-warning">Pay</span>" you agree to immediately access the service and to waive any right of withdrawal. You may terminate your subscription at any time by contact Taboor Service Center. The termination will be applied at the end of the current subscription period.</p>
                  
                   </Col>
                  <Col sm="5" className="text-center">
                    
                    <Card className="ml-5 mr-5">
                      <CardBody>
                      <div className="plan-info">
                        <h2><i className="fa fa-diamond text-warning"></i></h2>
                          <h4 className="mb-4"> Premium Plan Name</h4>
                          <h2 className="text-primary">200K $</h2>
                          <h6 className="text-warning mb-4">Per Year</h6>
                          <ul className="list-simple text-light-grey">
                            <li>1 month free Demo</li>
                            <li>Unlimited #Users</li>
                            <li>Unlimited #Branches</li>
                            <li>Easy Payment</li>
                            <li>Full year Support</li>
                          </ul>
                      </div>
                      </CardBody>
                    </Card>

                  </Col>
                </Row> 
              </Form> </CardBody>
              <CardFooter className="p-4 text-right b-a-0">
              <Button outline color="primary" onClick={()=>{this.nextStep(1)}}>Back</Button>
                <Button color="primary" className="ml-2" onClick={()=>{this.nextStep(2)}}>Play</Button>
              </CardFooter>
            </Card>
          )}

            </Col>

          <Col sm="3">
            <div className="p-5">
              <div className="login-logo mb-5">
                <AppNavbarBrand  full={{ src: logo, width: 250, alt: 'Taboor Logo' }} />
              </div>
              <Steps current={this.state.next}  direction="vertical">
                <Steps.Step title="Basic" description="Lorem ipsum dolor sit amet consectetur adipiscing elit, cupidatat non proident." />
                <Steps.Step title="Plan" description="Lorem ipsum dolor sit amet consectetur adipiscing elit, cupidatat non proident." />
                <Steps.Step title="Payment" description="Lorem ipsum dolor sit amet consectetur adipiscing elit, cupidatat non proident." />
              </Steps>
            </div>
          </Col>

        </Row>
        </Container>
      </div>
    );
  }
}

export default Register;

