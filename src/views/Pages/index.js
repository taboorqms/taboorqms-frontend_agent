import Login from './Landing';
import Login from './Login';
import Page404 from './Page404';
import Page500 from './Page500';
import Register from './Register';

export {
  Landing, Login, Page404, Page500, Register
};